﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataTypes
{
    [Serializable]
    public class cutsceneScript
    {
        [XmlElement]
        public List<string> script {get; set;}
    }
}
