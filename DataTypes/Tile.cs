﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataTypes
{
    [Serializable]
    class Tile
    {
        [XmlElement]
        bool isSolid;
        bool isTeleport;
        int tileSheetNumber;
    }
}
