﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataTypes
{
    [Serializable]
    public class Map
    {
        [XmlElement]
        public string name { get; set; }
        [XmlElement]
        public string textureName { get; set; }
        [XmlElement]
        public List<int> mapList { get; set; }
        [XmlElement]
        public string northMap { get; set; }
        [XmlElement]
        public string southMap { get; set; }
        [XmlElement]
        public string eastMap { get; set; }
        [XmlElement]
        public string westMap { get; set; }
    }
}
