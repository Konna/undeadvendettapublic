﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndeadVendetta
{
    class Quest
    {
        #region Declarations
        public enum QuestType { Hunt, Retrieve }
        QuestType type;
        int amount;
        Item retrieveItem;
        Enemy huntEnemy;
        string description;
        string title;
        bool areaSpecific;
        bool active;
        bool completed;
        string specificArea;
        #endregion

        #region Constructor
        public Quest(QuestType newType, int Amount, Item retrieveThis, Enemy huntThis, string setTitle, string setDescription, bool specific, string ifany)
        {
            type = newType;
            amount = Amount;
            retrieveItem = retrieveThis;
            huntEnemy = huntThis;
            title = setTitle;
            description = setDescription;
            areaSpecific = specific;
            specificArea = ifany;
        }
        #endregion

        #region Properties
        public QuestType Type
        {
            get { return type; }
        }
        public string Description
        {
            get { return description; }
        }
        public string Title
        {
            get { return title; }
        }
        public Enemy HuntEnemy
        {
            get { return huntEnemy; }
        }
        public Item RetrieveItem
        {
            get { return retrieveItem; }
        }
        public int Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }
        #endregion

    }
}
