﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndeadVendetta
{
    class QuestManager
    {
        #region Declarations
        static List<Quest> worldQuests = new List<Quest>();
        static List<Quest> playerQuests = new List<Quest>();
        #endregion

        #region Init
        public static void Init()
        {
            worldQuests.Clear();
            playerQuests.Clear();
        }
        #endregion

        #region Helper Methods
        //public static void checkHuntingQuests(string Name)
        //{
        //    for (int i = 0; i < playerQuests.Count; i++)
        //    {
        //        if ((playerQuests[i].Type == Quest.QuestType.Hunt) && (playerQuests[i].HuntEnemy))
        //        {
        //            if (playerQuests[i].Active)
        //            {
        //                playerQuests[i].Amount--;
        //            }
        //        }
        //    }
        //}

        public static void checkRetrieveQuests(string Name)
        {
            for (int i = 0; i < playerQuests.Count; i++)
            {
                if ((playerQuests[i].Type == Quest.QuestType.Retrieve) && (playerQuests[i].RetrieveItem.Name == Name))
                {
                    if (playerQuests[i].Active)
                    {
                        playerQuests[i].Amount--;
                    }
                }
            }
        }
        #endregion

        #region Update
        public static void Update()
        {
            for (int i = 0; i < playerQuests.Count; i++)
            {
                if (playerQuests[i].Active == false)
                {
                    if (playerQuests[i].Amount <= 0)
                        playerQuests[i].Active = false;
                }
            }
        }
        #endregion
    }
}
