﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    static class StatsManager
    {
        #region Declartaions

        private static Texture2D texture, barTexture;
        private static Rectangle healthRectangle;
        private static Rectangle healthTextureRectangle;
        private static Rectangle staminaRectangle;
        private static Rectangle staminaTextureRectangle;
        private static Rectangle experienceTextureRectangle;
        private static Rectangle experienceRectangle;

        #endregion

        #region Init
        public static void Init(Texture2D statsTexture, Texture2D BarTexture, Rectangle rectangleHealth, Vector2 location) 
        {
            texture = statsTexture; // Set the texture
            barTexture = BarTexture;
            healthRectangle = rectangleHealth; // Set the initial size of the health bar
            healthTextureRectangle = new Rectangle(0, 0, 9, 10); // set rectangle for health texture
            staminaRectangle = rectangleHealth; // Set the initial size of the stamina bar
            staminaTextureRectangle = new Rectangle(12, 0, 8, 10); // set rectangle for stamina texture
            experienceRectangle = rectangleHealth;
            experienceTextureRectangle = new Rectangle(24,0,9,10);

        }
        #endregion

        #region Draw and Update
        public static void Update(GameTime gameTime)
        {
            healthRectangle = new Rectangle(27, 493, ((200 / 100) * Player.CharSprite.Health), 30); // Updates the health bar to the size matching the players health
            staminaRectangle = new Rectangle(27, 546, ((200 /100) * (int)Player.stamina), 30); // Updates the stamina bar to the size matching the players stamina
            experienceRectangle = new Rectangle(305, 561, ((300 / 100) * Player.experience), 15); // Updates the exp bar to the size matching the players exp
        } 

        public static void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            int stringStamina = (int)Player.stamina;
            spriteBatch.Draw(barTexture, new Rectangle(0, 468, 800, 132), Color.White);
            spriteBatch.Draw(texture, healthRectangle, healthTextureRectangle, Color.White); // Draws the health bar
            spriteBatch.DrawString(font, Player.CharSprite.Health.ToString(), new Vector2(50, 495), Color.White);
            spriteBatch.Draw(texture, staminaRectangle, staminaTextureRectangle, Color.White); // Draws the stamina bar
            spriteBatch.DrawString(font, stringStamina.ToString(), new Vector2(50, 548), Color.White);
            spriteBatch.Draw(texture, experienceRectangle, experienceTextureRectangle, Color.White); // Draws the exp bar
            spriteBatch.DrawString(font, ("Level: " + Player.level.ToString()), new Vector2(410, 537), Color.White);
        }
        #endregion
    }
}
