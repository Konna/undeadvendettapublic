﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class Player
    {
        #region Declarations
        public static Entity CharSprite;

        private static Vector2 baseAngle = Vector2.Zero;
        public static float stamina = 100f;
        public static int experience = 0;
        public static int level = 1;
        public static int baseDamage = 10;
        public static int MGBCoin = 1000;
        public static GamePadState currentPadState;
        public static GamePadState prevPadState;
        private static Rectangle scrollArea = new Rectangle(150, 100, 500, 350);
        static KeyboardState currentState = Keyboard.GetState();
        static KeyboardState prevKeyState = Keyboard.GetState();
        private static float gameoverTime = 5f;
        private static float speedytime;

        #region Walking Directions

        private static Rectangle standLeft = new Rectangle(64, 0, 32, 32);
        private static Rectangle walkLeft = new Rectangle(96, 0, 32, 32);
        private static Rectangle standRight = new Rectangle(0, 0, 32, 32);
        private static Rectangle walkRight = new Rectangle(32, 0, 32, 32);
        private static Rectangle standDown = new Rectangle(128, 0, 32, 32);
        private static Rectangle walkDown = new Rectangle(160, 0, 32, 32);
        private static Rectangle standUp = new Rectangle(192, 0, 32, 32);
        private static Rectangle walkUp = new Rectangle(224, 0, 32, 32);

        #endregion

        #endregion

        #region Properties
        public static Vector2 PathingNodePosition
        {
            get
            {
                return TileMap.GetSquareAtPixel(CharSprite.WorldCenter);
            }
        }

        #endregion

        #region Init
        public static void Init(Vector2 worldLocation, Texture2D texture, int textureIndex, int numOfFrames)
        {
            speedytime = 90f;
            CharSprite = new Entity(worldLocation, texture, Vector2.Zero, textureIndex, numOfFrames, 100, speedytime);
            CharSprite.BoundingXPadding = 4;
            CharSprite.BoundingYPadding = 4;
            CharSprite.AnimateWhenStopped = false;
        }
        #endregion

        #region Helper Methods
        public static void takeDamage(int damage)
        {
            int newDamage;
            float damageReduction;
            int armourpecent = 0;
            for (int t = 0; t < Inventory.inventory.Count; t++)
            {
                if (Inventory.inventory[t].isEquipped)
                {
                    if (Inventory.inventory[t].Type == "Armour")
                    {
                        armourpecent = Inventory.inventory[t].ItemValue;
                    }
                }
            }
            damageReduction = ((float)(damage / 100) * armourpecent );
            newDamage = damage - (int)damageReduction;
            CharSprite.Health -= newDamage;
        }
        #endregion

        #region Input Handling
        private static Vector2 handleKeyboardMovement(KeyboardState keyState)
        {
            Vector2 keyMovement = Vector2.Zero;

            if ((keyState.IsKeyDown(Keys.LeftShift)) && (stamina > 0))
            {
                speedytime = 200f;
                CharSprite.speed = speedytime;
                stamina--;
                if ((int)stamina == 0)
                {
                    speedytime = 20f;
                    CharSprite.speed = speedytime;
                }
            }

            if (keyState.IsKeyUp(Keys.LeftShift))
            {
                speedytime = 90f;
                CharSprite.speed = speedytime;
                if (stamina < 20)
                {
                    speedytime = 20f;
                    CharSprite.speed = speedytime;
                }
                if (stamina < 100)
                {
                    stamina = stamina + 0.1f;
                }
            }

            if (keyState.IsKeyDown(Keys.W))
            {
                keyMovement.Y--;

                CharSprite.ClearFrames();
                CharSprite.AddFrame(standUp);
                CharSprite.AddFrame(walkUp);

            }
            if (keyState.IsKeyDown(Keys.A))
            {
                keyMovement.X--;
                CharSprite.ClearFrames();
                CharSprite.AddFrame(standLeft);
                CharSprite.AddFrame(walkLeft);
            }
            if (keyState.IsKeyDown(Keys.S))
            {
                keyMovement.Y++;

                CharSprite.ClearFrames();
                CharSprite.AddFrame(standDown);
                CharSprite.AddFrame(walkDown);

            }
            if (keyState.IsKeyDown(Keys.D))
            {
                keyMovement.X++;
                CharSprite.ClearFrames();
                CharSprite.AddFrame(standRight);
                CharSprite.AddFrame(walkRight);
            }
            return keyMovement;
        }
        private static void HandleKeyboardInput(KeyboardState keyState, KeyboardState preState, float elapsed)
        {
            for (int i = 0; i < EnemyManager.levelEnemies.Count; i++)
            {
                if (EnemyManager.levelEnemies[i].GetType() == typeof(Sheep) || EnemyManager.levelEnemies[i].GetType() == typeof(TavernKeep))
                {
                    continue;
                }
                else
                {
                    if ((Entity.IsRectOccupied((new Rectangle((CharSprite.BoundingBoxRect.X + 32), (CharSprite.BoundingBoxRect.Y + 32), 32, 32)), EnemyManager.levelEnemies[i].enemySprite)) ||
                        (Entity.IsRectOccupied((new Rectangle((CharSprite.BoundingBoxRect.X - 32), (CharSprite.BoundingBoxRect.Y - 32), 32, 32)), EnemyManager.levelEnemies[i].enemySprite)))
                    {
                        if (keyState.IsKeyDown(Keys.Space) && (preState.IsKeyUp(Keys.Space)))
                        {
                            int addedDamage = 0;
                            for (int t = 0; t < Inventory.inventory.Count; t++)
                            {
                                if (Inventory.inventory[t].isEquipped)
                                {
                                    if (Inventory.inventory[t].Type == "Weapon")
                                    {
                                        addedDamage = Inventory.inventory[t].ItemValue;
                                    }
                                }
                            }
                            EnemyManager.levelEnemies[i].enemySprite.Health -= (baseDamage + addedDamage);
                            continue;
                        }

                    }
                }
            }
        }
        private static Vector2 handleGamePadMovement(GamePadState gamepadState)
        {
            Vector2 PadMovement = Vector2.Zero;
            if (gamepadState.DPad.Up == ButtonState.Pressed)
            {
                PadMovement.Y--;

                CharSprite.ClearFrames();
                CharSprite.AddFrame(standUp);
                CharSprite.AddFrame(walkUp);

            }
            if (gamepadState.DPad.Left == ButtonState.Pressed)
            {
                PadMovement.X--;
                CharSprite.ClearFrames();
                CharSprite.AddFrame(standLeft);
                CharSprite.AddFrame(walkLeft);
            }
            if (gamepadState.DPad.Down == ButtonState.Pressed)
            {
                PadMovement.Y++;

                CharSprite.ClearFrames();
                CharSprite.AddFrame(standDown);
                CharSprite.AddFrame(walkDown);

            }
            if (gamepadState.DPad.Right == ButtonState.Pressed)
            {
                PadMovement.X++;
                CharSprite.ClearFrames();
                CharSprite.AddFrame(standRight);
                CharSprite.AddFrame(walkRight);
            }
            return PadMovement;
        }
        private static void HandleGamePadInput(GamePadState gamepadState, GamePadState preState, float elapsed)
        {
            for (int i = 0; i < EnemyManager.levelEnemies.Count; i++)
            {
                if ((Entity.IsRectOccupied((new Rectangle((CharSprite.BoundingBoxRect.X + 32), (CharSprite.BoundingBoxRect.Y + 32), 32, 32)), EnemyManager.levelEnemies[i].enemySprite)) ||
                    (Entity.IsRectOccupied((new Rectangle((CharSprite.BoundingBoxRect.X - 32), (CharSprite.BoundingBoxRect.Y - 32), 32, 32)), EnemyManager.levelEnemies[i].enemySprite)))
                {
                    if ((gamepadState.Triggers.Right == 1) && (preState.Triggers.Right == 0))
                    {
                        int addedDamage = 0;
                        for (int t = 0; t < Inventory.inventory.Count; t++)
                        {
                            if (Inventory.inventory[t].isEquipped)
                            {
                                if (Inventory.inventory[t].Type == "Weapon")
                                {
                                    addedDamage = Inventory.inventory[t].ItemValue;
                                }
                            }
                        }
                        EnemyManager.levelEnemies[i].enemySprite.Health -= (baseDamage + addedDamage);
                        continue;
                    }

                }
            }
        }
        private static void handleInput(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Vector2 moveAngle = Vector2.Zero;
            Vector2 fireAngle = Vector2.Zero;
            currentState = Keyboard.GetState();
            currentPadState = GamePad.GetState(PlayerIndex.One);
            moveAngle += handleKeyboardMovement(Keyboard.GetState());
            moveAngle += handleGamePadMovement(GamePad.GetState(PlayerIndex.One));
            HandleKeyboardInput(currentState, prevKeyState, elapsed);
            HandleGamePadInput(currentPadState, prevPadState, elapsed);

            if (moveAngle != Vector2.Zero)
            {
                moveAngle.Normalize();
                moveAngle = CharSprite.checkTileObstacles(elapsed, moveAngle);

                foreach (Enemy i in EnemyManager.levelEnemies)
                {
                    moveAngle = checkIfColliidingWithPlayer(elapsed, moveAngle, i.enemySprite);
                }
            }

            prevKeyState = currentState;
            prevPadState = currentPadState;
            CharSprite.Velocity = moveAngle * CharSprite.speed;
            repositionCamera(gameTime, moveAngle);
        }
        #endregion

        #region Movement Limitations
        private static void clampToWorld()
        {
            float currentX = CharSprite.WorldLocation.X;
            float currentY = CharSprite.WorldLocation.Y;

            currentX = MathHelper.Clamp(
                currentX,
                0,
                Camera.WorldRectangle.Right - CharSprite.FrameWidth);

            currentY = MathHelper.Clamp(
                currentY,
                0,
                Camera.WorldRectangle.Bottom - CharSprite.FrameHeight);

            CharSprite.WorldLocation = new Vector2(currentX, currentY);
        }

        private static void repositionCamera(
            GameTime gameTime,
            Vector2 moveAngle)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            float moveScale = CharSprite.speed * elapsed;

            if ((CharSprite.ScreenRectangle.X < scrollArea.X) &&
                (moveAngle.X < 0))
            {  
                if ((CharSprite.WorldLocation.X <= 150))
            {
                Camera.Move(Vector2.Zero);
                }
                else
                {
                Camera.Move(new Vector2(moveAngle.X, 0) * moveScale);
                }
            }

            if ((CharSprite.ScreenRectangle.Right > scrollArea.Right) &&
                (moveAngle.X > 0))
            {
                if ((CharSprite.WorldLocation.X >= 619))
                {
                    Camera.Move(Vector2.Zero);
                }
                else
                {
                    Camera.Move(new Vector2(moveAngle.X, 0) * moveScale);
                }
            }

            if ((CharSprite.ScreenRectangle.Y < scrollArea.Y) &&
                (moveAngle.Y < 0))
            {
                if ((CharSprite.WorldLocation.Y <= 99))
                {
                    Camera.Move(Vector2.Zero);
                }
                else
                {
                    Camera.Move(new Vector2(0, moveAngle.Y) * moveScale);
                }
            }

            if ((CharSprite.ScreenRectangle.Bottom > scrollArea.Bottom) &&
                (moveAngle.Y > 0))
            {
                Camera.Move(new Vector2(0, moveAngle.Y) * moveScale);
            }

          


        }
        private static bool CollidingWithEnemy(float elapsedTime, Vector2 moveAngle, Entity enemy)
        {
            return (checkIfColliidingWithPlayer(elapsedTime, moveAngle, enemy) == new Vector2(0, 0));
        }
        private static Vector2 checkIfColliidingWithPlayer(float elapsedTime, Vector2 moveAngle, Entity enemy)
        {
            Vector2 newHorizontalLocation = CharSprite.WorldLocation + (new Vector2(moveAngle.X, 0) *
                (CharSprite.speed * elapsedTime));
            Vector2 newVerticalLocation = CharSprite.WorldLocation + (new Vector2(0, moveAngle.Y) *
                (CharSprite.speed * elapsedTime));

            Rectangle newHorizontalRect = new Rectangle((int)newHorizontalLocation.X,
                (int)CharSprite.WorldLocation.Y, CharSprite.FrameWidth, CharSprite.FrameHeight);
            Rectangle newVerticalRect = new Rectangle((int)CharSprite.WorldLocation.X,
                (int)newVerticalLocation.Y, CharSprite.FrameWidth, CharSprite.FrameHeight);

            if (moveAngle.X != 0)
            {

                if (Entity.IsRectOccupied(newHorizontalRect, enemy))
                {
                    moveAngle.X = 0;
                }

            }

            if (moveAngle.Y != 0)
            {
                if (Entity.IsRectOccupied(newVerticalRect, enemy))
                {
                    moveAngle.Y = 0;
                }
            }

            return moveAngle;
        }
        public static bool IsRectOccupied(Rectangle otherPlayer)
        {
            Rectangle playerRect = new Rectangle((int)CharSprite.WorldLocation.X,
               (int)CharSprite.WorldLocation.Y, CharSprite.FrameWidth, CharSprite.FrameHeight);
            return otherPlayer.Intersects(playerRect);
        }
        #endregion

        #region Update and Draw
        public static void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            handleInput(gameTime);
            if (CharSprite.Expired == false)
            {
                if (CharSprite.Health <= 0)
                {
                    CharSprite.Expired = true;
                }
                CharSprite.Update(gameTime);
                if (experience >= 100)
                {
                    int savedexp = experience - 100;
                    level += 1;
                    baseDamage += 5;
                    experience = 0 + savedexp;
                }
            }
            if (CharSprite.Expired == true)
            {
                gameoverTime -= elapsed;
            }
        }

        public static void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            if (CharSprite.Expired == false)
            {

                if (CharSprite.Health <= 0)
                {
                    CharSprite.Expired = true;
                }
                CharSprite.Draw(spriteBatch);
            }
            if (CharSprite.Expired == true)
            {
                spriteBatch.DrawString(font, "Game Over You Died", new Vector2(250, 250), Color.Teal);
                if (gameoverTime < 0.0f)
                {
                    Game1.currentGameState = Game1.gameState.GameOver;
                }
            }
        }
        #endregion

    }
}
