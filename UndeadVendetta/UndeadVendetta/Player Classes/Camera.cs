﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    public static class Camera
    {
        #region Declarations
        private static Vector2 position = Vector2.Zero;
        private static Vector2 viewPortSize = Vector2.Zero;
        private static Rectangle worldRectangle = new Rectangle(0, 0, 0, 0);
        #endregion

        #region Properties
        public static Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = new Vector2(MathHelper.Clamp(value.X, worldRectangle.X,
                    worldRectangle.Width - ViewPortWidth),
                    MathHelper.Clamp(value.Y, worldRectangle.Y,
                    worldRectangle.Height - ViewPortHeight));
            }
        }

        public static Rectangle WorldRectangle
        {
            get { return worldRectangle; }
            set { worldRectangle = value; }
        }

        public static int ViewPortWidth
        {
            get { return (int)viewPortSize.X; }
            set { viewPortSize.X = value; }
        }

        public static int ViewPortHeight
        {
            get { return (int)viewPortSize.Y; }
            set { viewPortSize.Y = value; }
        }

        public static Rectangle ViewPort
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, ViewPortWidth, ViewPortHeight);
            }
        }
        #endregion

        #region Public Methods
        public static void Move(Vector2 offset)
        {
            position += offset;
        }
        public static bool ObjectIsVisible(Rectangle bounds)
        {
            return (ViewPort.Intersects(bounds));
        }
        public static Vector2 Transform(Vector2 point)
        {
            return point - position;
        }
        public static Rectangle Transform(Rectangle rectangle)
        {
            return new Rectangle(rectangle.Left - (int)position.X, rectangle.Top - (int)position.Y,
                rectangle.Width, rectangle.Height);
        }
        public static void jumpToPlayer()
        {
            position.X = Player.CharSprite.WorldCenter.X + ViewPortWidth / 2;
            position.Y = Player.CharSprite.WorldCenter.X + ViewPortHeight / 2;
        }
        public static void keepInBounds()
        {
            //if player location is within a certain range of the edge of the map then we ti
        }
        #endregion
    }
}
