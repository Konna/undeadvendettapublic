﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    public class Inventory
    {
        #region Declarations
        public static List<Item> items = new List<Item>();
        public static  List<Item> inventory = new List<Item>();
        #endregion

        #region Init
        public static void init(Texture2D newTexture)
        {
            items.Clear();
            inventory.Clear();
            items.Add(new Item("Weapon","Sword", 100, 1000, 10, "health"));
            items.Add(new Item("Weapon","Axe", 100, 1100, 15, "health"));
            items.Add(new Item("Weapon","Claymore", 300, 1300, 15, "health"));
            items.Add(new Item("Weapon", "Broadsword", 500, 1500, 23,"health" ));
            items.Add(new Item("Weapon", "Broadsword", 500, 1500, 23, "health"));
            items.Add(new Item("Weapon", "Gladius", 700, 1700, 30, "health"));
            items.Add(new Item("Weapon", "Katana", 1000, 2000, 40, "health"));
            items.Add(new Item("Weapon", "LongSword", 1200, 2200, 50, "health"));

            items.Add(new Item("Armour", "Wooden Helm", 300, 1300, 10, "health"));
            items.Add(new Item("Armour", "Iron Helm", 700, 1700, 25, "health"));
            items.Add(new Item("Armour", "Mithril Helm", 1000, 2000, 40,"health" ));
            items.Add(new Item("Armour", "Wooden Plate", 300, 1300, 10,"health" ));
            items.Add(new Item("Armour", "Iron Plate", 700, 1700, 30, "health"));
            items.Add(new Item("Armour", "Mithril Plate", 1000, 2000, 50,"health" ));
            items.Add(new Item("Armour", "Wooden Chaps", 300, 1300, 10,"health" ));
            items.Add(new Item("Armour", "Iron Chaps", 700, 1700, 25,"health" ));
            items.Add(new Item("Armour", "Mithril Chaps", 1000, 2000, 40,"health" ));

            items.Add(new Item("Consumable", "Small Health Potion", 100, 1000, 10,"health" ));
            items.Add(new Item("Consumable", "Health Potion", 300, 1300, 25,"health" )); // made with tiger blood
            items.Add(new Item("Consumable", "Large Health Potion", 500, 1500, 50,"health" ));

            items.Add(new Item("Lemon", "Lemon", 100, 100, 10, "health"));
        }
        #endregion

        #region Helper Methods
        public static int findItemIndexViaString(string name)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if(items[i].Name == name)
                {
                return i;
                }
            }
            return -1;
        }
        public static int findInventoryIndexViaString(string name)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].Name == name)
                    return i;
            }
            return -1;
        }
        public static void addToInventory(string name)
        {
            int itemIndex = findItemIndexViaString(name);
            if (itemIndex == -1)
                return;
            else
                if (inventory.Count == 16)
                    return;
                else
                inventory.Add(items[itemIndex]);
        }
        public static void removeFromInventory(string name)
        {
            int itemIndex = findInventoryIndexViaString(name);
            if (itemIndex == -1)
                return;
            else
                inventory.Remove(inventory[itemIndex]);
        }
        public static void removeFromInventory(int index)
        {
                inventory.Remove(inventory[index]);
        }
        public static void equipArmour(string name)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].Type == "Armour")
                {
                    if (inventory[i].isEquipped)
                        inventory[i].isEquipped = false;
                }
            }
            int itemIndex = findInventoryIndexViaString(name);
            inventory[itemIndex].isEquipped = true;
        }
        public static void equipWeapon(string name)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].Type == "Weapon")
                {
                    if (inventory[i].isEquipped)
                        inventory[i].isEquipped = false;
                }
            }
            int itemIndex = findInventoryIndexViaString(name);
            inventory[itemIndex].isEquipped = true;
        }
        public static void equipArmour(int index)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].Type == "Armour")
                {
                    if (inventory[i].isEquipped)
                        inventory[i].isEquipped = false;
                }
            }
            inventory[index].isEquipped = true;
        }
        public static void equipWeapon(int index)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].Type == "Weapon")
                {
                    if (inventory[i].isEquipped)
                        inventory[i].isEquipped = false;
                }
            }
            inventory[index].isEquipped = true;
        }
        public static Item selectRandomDrop()
        {
            Random rand = new Random();
            Item drop;
            do
            {
                drop = items[rand.Next(0, items.Count - 1)];
            } while (inventory.Contains(drop) == true);
            return drop;
        }
        public static void refineItem(int item)
        {
            int refineCost = 200;
            if (Player.MGBCoin < refineCost)
            {
                return;
            }
            else
            {
                Random random = new Random();
                int refineValue = random.Next(1,5);
                Item refinedItem = new Item();
                refinedItem = inventory[item];
                refinedItem.ItemValue += refineValue;
                removeFromInventory(item);
                inventory.Add(refinedItem);
                Player.MGBCoin -= 200;
            }
        }


        #endregion

        #region Properties
        public static string EquippedArmour
        {
            get
            {
                for (int i = 0; i < inventory.Count; i++)
                {
                    if (inventory[i].isEquipped == true)
                    {
                       if (inventory[i].Type == "Armour")
                        {
                            return inventory[i].Name;
                        }
                    }
                }
                return "";
            }
        }

        public static string EquippedWeapon
        {
            get
            {
                for (int i = 0; i < inventory.Count; i++)
                {
                    if (inventory[i].isEquipped == true)
                    {
                        if (inventory[i].Type == "Weapon")
                        {
                            return inventory[i].Name;
                        }
                    }
                }
                return "";
            }
        }

        public static bool ContainsLemon
        {
            get
            {
                for (int i = 0; i < inventory.Count; i++)
                {
                    if (inventory[i].Name == "Lemon")
                        return true;
                }
                return false;
            }
        }

        #endregion
    }
}
