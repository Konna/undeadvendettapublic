﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UndeadVendetta
{
    class GameItemManager
    {
        #region Declaration
        readonly Dictionary<string, GameItem> gameItems = new Dictionary<string, GameItem>();
        static SpriteFont spriteFont;
        #endregion

        #region Properties
        public Dictionary<string, GameItem> GameItems
        {
            get { return gameItems; }
        }
        public static SpriteFont SpriteFont
        {
            get { return spriteFont; }
            private set { spriteFont = value; }
        }
        #endregion

        #region Constructor
        public GameItemManager(SpriteFont spriteFont)
        {
            SpriteFont = spriteFont;
        }
        #endregion

        #region Method 
        #endregion

        #region Virtual Method 
        #endregion


    }
}
