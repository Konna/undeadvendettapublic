﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Xml.Serialization;

namespace UndeadVendetta
{
    public class Item
    {
        #region Decalrations
        string name = "";
        string type = "";
        int itemValue = 0;
        string consumeType = "";
        int price = 0;
        int sellPrice = 0;
        bool equipped = false;
        #endregion

        #region Constructor
        public Item(string setType, string setName, int setPrice, int setSellPrice, int value, string setConsumeType)
        {
            name = setName;
            price = setPrice;
            sellPrice = setSellPrice;
            type = setType;
            itemValue = value;
            consumeType = setType;
        }
        public Item()
        {
            name = "";
            type = "";
            itemValue = 0;
            consumeType = "";
            price = 0;
            sellPrice = 0;
        }
        #endregion

        #region Properties
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int ItemValue
        { 
            get { return itemValue; }
            set { itemValue = value; }
        }

        public string Type
        {
            get { return type; }
        }
        public bool isEquipped
        {
            get { return equipped; }
            set { equipped = value; }
        }
        #endregion
        
    }
}
