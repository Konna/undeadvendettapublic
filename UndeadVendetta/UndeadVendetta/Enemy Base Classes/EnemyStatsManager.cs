﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    static class EnemyStatsManager
    {
        #region Declartaions

        private static Texture2D texture;
        private static List<Rectangle> healthRectangles = new List<Rectangle>();
        private static Rectangle healthTextureRectangle, RectangleHealth;

        #endregion

        #region Init
        public static void Init(Texture2D statsTexture, Rectangle rectangleHealth, Vector2 location)
        {
            texture = statsTexture; // Set the texture
            healthRectangles.Clear();
            RectangleHealth = rectangleHealth;
            for (int i = 0; i < EnemyManager.levelEnemies.Count; i++)
            {
                healthRectangles.Add(rectangleHealth);
            }
            healthTextureRectangle = new Rectangle(0, 0, 9, 10); // set rectangle for health texture
        }
        #endregion
        #region Change level enemies
        public static void changeLevelEnemies()
        {
            healthRectangles.Clear();
            for (int i = 0; i < EnemyManager.levelEnemies.Count; i++)
            {
                if (EnemyManager.levelEnemies[i].GetType() != typeof(Sheep) && EnemyManager.levelEnemies[i].GetType() != typeof(TavernKeep) && EnemyManager.levelEnemies[i].GetType() != typeof(BlackSmith))
                {
                    healthRectangles.Add(RectangleHealth);
                }
            }
        }
        #endregion

        #region Draw and Update
        public static void Update(GameTime gameTime)
        {
            for (int i = 0; i < EnemyManager.levelEnemies.Count; i++)
            {
                if (EnemyManager.levelEnemies[i].GetType() != typeof(Sheep) && EnemyManager.levelEnemies[i].GetType() != typeof(TavernKeep) && EnemyManager.levelEnemies[i].GetType() != typeof(BlackSmith))
                {
                    healthRectangles[i] = new Rectangle((int)EnemyManager.levelEnemies[i].enemySprite.ScreenLocation.X, (int)EnemyManager.levelEnemies[i].enemySprite.ScreenLocation.Y + 50, (int)(EnemyManager.levelEnemies[i].enemySprite.Health * 0.35), 10); // Updates the health bar to the size matching the players health
                }
            }
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < EnemyManager.levelEnemies.Count; i++)
            {
                if (!EnemyManager.levelEnemies[i].enemySprite.Expired)
                {
                    if (EnemyManager.levelEnemies[i].GetType() != typeof(Sheep) && EnemyManager.levelEnemies[i].GetType() != typeof(TavernKeep) && EnemyManager.levelEnemies[i].GetType() != typeof(BlackSmith))
                    {
                        spriteBatch.Draw(texture, healthRectangles[i], healthTextureRectangle, Color.White); // Draws the health bar
                    }
                }
            }
        }
        #endregion
    }
}
