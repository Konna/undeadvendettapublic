﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace UndeadVendetta
{
    class EnemyManager
    {
        #region Declarations
        private static Texture2D slimeTexture, sheepTexture, crabTexture, bearTexture, tavernKeepSheet, blacksmithSheet;
        public static  SoundEffect SlimeEffect;
        public static List<Enemy> enemies = new List<Enemy>();
        public static List<Enemy> levelEnemies = new List<Enemy>();
        #endregion

        #region Init
        public static void Init(Texture2D setSlimeTexture, Texture2D setCrabTexture, Texture2D setSheepTexture, Texture2D setBearTexture, Texture2D setTavernKeep, Texture2D setBlackSmith, SoundEffect slimeEffect)
        {
            slimeTexture = setSlimeTexture;
            crabTexture = setCrabTexture;
            bearTexture = setBearTexture;
            sheepTexture = setSheepTexture;
            tavernKeepSheet = setTavernKeep;
            blacksmithSheet = setBlackSmith;
            SlimeEffect = slimeEffect;
            enemies.Clear();
            levelEnemies.Clear();
            GenerateTrainingEnemies();
        }
        #endregion

        #region Helper Methods
        public static void GenerateTrainingEnemies()
        {
            levelEnemies.Clear();
            levelEnemies.Add(new Slime(new Vector2(250, 250), slimeTexture, 0, 2, 100, 10, 5, 30f, SlimeEffect));
            levelEnemies.Add(new Slime(new Vector2(350, 250), slimeTexture, 0, 2, 100, 10, 5, 30f, SlimeEffect));
            levelEnemies.Add(new Slime(new Vector2(450, 250), slimeTexture, 0, 2, 100, 10, 5, 30f, SlimeEffect));
            EnemyStatsManager.changeLevelEnemies();
        }
        public static void GenerateBeachEnemies()
        {
            levelEnemies.Clear();
            levelEnemies.Add(new Crab(new Vector2(400, 350), crabTexture, 3, 2, 50, 14, 10, 30f, SlimeEffect));
            levelEnemies.Add(new Crab(new Vector2(400, 400), crabTexture, 3, 2, 50, 14, 10, 30f, SlimeEffect));
            levelEnemies.Add(new Crab(new Vector2(400,450), crabTexture, 3, 2, 50, 14, 10, 30f, SlimeEffect));
            levelEnemies.Add(new Crab(new Vector2(400, 500), crabTexture, 3, 2, 50, 14, 10, 30f, SlimeEffect));
            EnemyStatsManager.changeLevelEnemies();
        }
        public static void GenerateTownSheep()
        {
            levelEnemies.Clear();
            levelEnemies.Add(new TavernKeep(new Vector2(150, 200), tavernKeepSheet, 0, 2, 100, 0, 0, 30f, null));
            levelEnemies.Add(new Sheep(new Vector2(300, 400), sheepTexture, 0, 2, 100, 0, 0, 40f, null));
        }
        public static void GenerateDungeonBoss()
        {
            levelEnemies.Clear();
            levelEnemies.Add(new Bear(new Vector2(500,250),bearTexture,0,2,150,100,40,30f,null));
            levelEnemies.Add(new BlackSmith(new Vector2(560, 250), blacksmithSheet, 0, 2, 100, 0, 0, 30f, null));
            EnemyStatsManager.changeLevelEnemies();
        }
        public static bool allEnemiesClear()
        {
            for (int i = 0; i < levelEnemies.Count; i++)
            {
                if (levelEnemies[i].GetType() != typeof(Sheep) && levelEnemies[i].GetType() != typeof(TavernKeep) && levelEnemies[i].GetType() != typeof(BlackSmith))
                {
                    if (levelEnemies[i].enemySprite.Expired == false) return false;
                }
            }
            return true;

        }
        #endregion

        #region Update and Draw
        public static void Update(GameTime gameTime, GraphicsDevice graphics)
        {
            for (int i = 0; i < levelEnemies.Count; i++ )
            {
                if (!levelEnemies[i].enemySprite.Expired)
                {
                    if (levelEnemies[i].enemySprite.Health <= 0)
                    {
                        levelEnemies[i].death();
                        continue;
                    }
                    levelEnemies[i].update(gameTime);
                }
            }
        }
        public static void Draw(SpriteBatch spriteBatch)
        {
            foreach (Enemy i in levelEnemies)
            {
                i.draw(spriteBatch);
            }
        }
        #endregion

    }
}
