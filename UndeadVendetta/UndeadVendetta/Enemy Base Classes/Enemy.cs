﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace UndeadVendetta
{
     class  Enemy
    {
        #region Declarations
        public Entity enemySprite;
        private Texture2D baseTexture;
        public Vector2 currentTargetSquare;
        public bool destroyed = false;
        private int collisionRadius = 14;
        public int experiance;
        public float timemax = 0.5f, timecurrent, attackTimeMax = 1.0f, attackTimeCurrent;
        public SoundEffect effect;
        public int damage;
        #endregion

        #region Constructor
        public Enemy(
            Vector2 worldLocation, Texture2D texture, int textureIndex, int frames, int health, int setExperiance, int setDamage, float setSpeed, SoundEffect effectSound)
        {
            enemySprite = new Entity(worldLocation,  texture,  Vector2.Zero,  textureIndex,  frames,  health, setSpeed);
            baseTexture = texture;
            experiance = setExperiance;
            damage = setDamage;
            currentTargetSquare = new Vector2(100,100);
            enemySprite.CollisionRadius = collisionRadius;
            effect = effectSound;
        }
        #endregion
        
        #region Public Methods
        public void death()
        {
            Item drop = Inventory.selectRandomDrop();
            Inventory.addToInventory(drop.Name);
            enemySprite.Expired = true;
            Player.experience += experiance;
        }
        
        #endregion

        #region Update & Draw
        public virtual void update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timecurrent += elapsed;
            attackTimeCurrent += elapsed;
            if (!enemySprite.Expired)
            {
                if (enemySprite.Health <= 0)
                {
                    enemySprite.Expired = true;
                }
                enemySprite.Update(gameTime);
            }
        }

        public virtual void draw(SpriteBatch spriteBatch)
        {
            if (!destroyed)
            {
                enemySprite.Draw(spriteBatch);
            }
        }
        #endregion
    }
}
