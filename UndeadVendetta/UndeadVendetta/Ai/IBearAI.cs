﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndeadVendetta
{
    interface IBearAI
    {
        void chase(Entity enemySprite);
        void attack(int damage);
        void retreat(Entity enemySprite, float elapsedTime);
        void regen(int amount, float duration, float elapsed, Entity enemySprite);
    }
}
