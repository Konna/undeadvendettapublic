﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    public class AICore
    {
        
        public Vector2 determineMoveDirection(Entity enemyBase)
        {
            Vector2 currentTargetSquare = Vector2.Zero;
            bool firstSkip = true;
            if (firstSkip)
            {
                currentTargetSquare = getNewTargetSquare(enemyBase);
                firstSkip = true;
                Vector2 squareCenter1 = TileMap.GetSquareCenter(currentTargetSquare);
                return squareCenter1 - enemyBase.WorldCenter;
            }
            if (reachedTargetSquare(enemyBase, currentTargetSquare))
            {
                currentTargetSquare = getNewTargetSquare(enemyBase);
            }
            Vector2 squareCenter = TileMap.GetSquareCenter(currentTargetSquare);
            return squareCenter - enemyBase.WorldCenter;
        }
        bool reachedTargetSquare(Entity enemyBase, Vector2 currentTargetSquare)
        {
            return (Vector2.Distance(
                enemyBase.WorldLocation,
                TileMap.GetSquareCenterForEnemy(currentTargetSquare)) >= 10);
        }
        Vector2 getNewTargetSquare(Entity enemyBase)
        {
            List<Vector2> path = new List<Vector2>();
            path = PathFinder.FindPath(
                 TileMap.GetSquareAtPixelForEnemy(enemyBase.WorldCenter),
                 TileMap.GetSquareAtPixelForEnemy(Player.CharSprite.WorldLocation));
            if (Player.CharSprite.WorldLocation.X < 50 || Player.CharSprite.WorldLocation.Y < 50 || Player.CharSprite.WorldLocation.Y > 500)
            {
                return TileMap.GetSquareAtPixelForEnemy(Player.CharSprite.WorldLocation);
            }
            if (path.Count > 1)
            {
                return new Vector2(path[1].X, path[1].Y);
            }
            else
            {
                return TileMap.GetSquareAtPixelForEnemy(Player.CharSprite.WorldLocation);
            }
        }
    }
}
