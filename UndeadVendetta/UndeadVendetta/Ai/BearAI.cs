﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    class BearAI : IBearAI
    {
        #region Declarations
        AICore core = new AICore();
        private static Rectangle standLeft = new Rectangle(64, 0, 32, 32);
        private static Rectangle walkLeft = new Rectangle(96, 0, 32, 32);
        private static Rectangle standRight = new Rectangle(0, 0, 32, 32);
        private static Rectangle walkRight = new Rectangle(32, 0, 32, 32);
        private static Rectangle standDown = new Rectangle(128, 0, 32, 32);
        private static Rectangle walkDown = new Rectangle(160, 0, 32, 32);
        private static Rectangle standUp = new Rectangle(192, 0, 32, 32);
        private static Rectangle walkUp = new Rectangle(224, 0, 32, 32);
        private static Rectangle standDownLeft = new Rectangle(256, 0, 32, 32);
        private static Rectangle walkDownLeft = new Rectangle(288, 0, 32, 32);
        #endregion

        #region AI Handling
        void IBearAI.chase(Entity enemySprite)
        {
            Vector2 direction = core.determineMoveDirection(enemySprite);
            direction.Normalize();
            if (direction.X > 0)
            {
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standRight);
                enemySprite.AddFrame(walkRight);
            }
            if (direction.X < 0)
            {
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standLeft);
                enemySprite.AddFrame(walkLeft);
            }
            if (direction.Y > 0)
            {
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standDown);
                enemySprite.AddFrame(walkDown);
            }
            if (direction.Y < 0)
            {
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standUp);
                enemySprite.AddFrame(walkUp);
            }
            //if (direction.Y > 0 && direction.X < 0)
            //{
            //    enemySprite.ClearFrames();
            //    enemySprite.AddFrame(standDownLeft);
            //    enemySprite.AddFrame(walkDownLeft);
            //}
            if (!(Entity.IsRectOccupied(Player.CharSprite.worldRectangle, enemySprite)))
            {
                enemySprite.Velocity = direction * enemySprite.speed;
                //if ((timemax - timecurrent) <= 0.0)
                //{
                //    effect.Play();
                //    timecurrent = 0.0f;
                //}
            }
        }
        void IBearAI.attack(int damage)
        {
            Player.takeDamage(damage);
        }
        void IBearAI.retreat(Entity enemySprite, float elapsedTime)
        {
            Vector2 moveAngle = Vector2.Zero;
            if ((enemySprite.WorldLocation.X - Player.CharSprite.WorldLocation.X) >= 0) // Enemy to the right
            {
                moveAngle.X++;
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standRight);
                enemySprite.AddFrame(walkRight);
            }
            else if ((enemySprite.WorldLocation.X - Player.CharSprite.WorldLocation.X) < 0) // Enemy to the left
            {
                moveAngle.X--;
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standLeft);
                enemySprite.AddFrame(walkLeft);
            }
            if ((enemySprite.WorldLocation.Y - Player.CharSprite.WorldLocation.Y) >= 0) // Enemy is below
            {
                moveAngle.Y++;
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standDown);
                enemySprite.AddFrame(walkDown);
            }
            else if ((enemySprite.WorldLocation.Y - Player.CharSprite.WorldLocation.Y) < 0) // Enemy is above
            {
                moveAngle.Y--;
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standUp);
                enemySprite.AddFrame(walkUp);
            }
            if (moveAngle != Vector2.Zero)
            {
                moveAngle.Normalize();
                moveAngle = enemySprite.checkTileObstacles(elapsedTime, moveAngle);
            }
            enemySprite.Velocity = moveAngle * enemySprite.speed;
        }
        void IBearAI.regen(int amount, float duration, float elapsed, Entity enemySprite)
        {
            int currentHealth = enemySprite.Health;
            int helthpersecond = amount / (int)duration;


        }
        #endregion
    }
}
