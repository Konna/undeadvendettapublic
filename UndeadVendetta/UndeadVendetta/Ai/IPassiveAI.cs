﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    public interface IPassiveAI
    {
        void attack(int damage);
        void retreat(Entity enemySprite, float elapsedTime);
        void follow(Entity enemySprite);
        void wander();
    }
}
