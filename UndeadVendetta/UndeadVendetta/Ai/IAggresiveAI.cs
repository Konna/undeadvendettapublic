﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndeadVendetta
{
    public interface IAggressiveAI
    {
        void attack(int damage);
        void chase(Entity enemySprite);
        void wander();
    }
}
