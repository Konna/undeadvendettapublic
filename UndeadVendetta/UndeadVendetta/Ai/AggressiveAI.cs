﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    class AggressiveAI : IAggressiveAI
    {
        #region Declarations
        AICore core = new AICore();
        private static Rectangle standLeft = new Rectangle(64, 0, 32, 32);
        private static Rectangle walkLeft = new Rectangle(96, 0, 32, 32);
        private static Rectangle standRight = new Rectangle(0, 0, 32, 32);
        private static Rectangle walkRight = new Rectangle(32, 0, 32, 32);
        private static Rectangle standDown = new Rectangle(128, 0, 32, 32);
        private static Rectangle walkDown = new Rectangle(160, 0, 32, 32);
        private static Rectangle standUp = new Rectangle(192, 0, 32, 32);
        private static Rectangle walkUp = new Rectangle(224, 0, 32, 32);
        private static Rectangle standDownLeft = new Rectangle(256, 0, 32, 32);
        private static Rectangle walkDownLeft = new Rectangle(288, 0, 32, 32);
        #endregion

        #region AIHandling
        void IAggressiveAI.attack(int damage)
        {
            Player.takeDamage(damage);
        }
        void IAggressiveAI.chase(Entity enemySprite)
        {
            Vector2 direction = core.determineMoveDirection(enemySprite);
            direction.Normalize();
            if (direction.X > 0)
            {
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standRight);
                enemySprite.AddFrame(walkRight);
            }
            if (direction.X < 0)
            {
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standLeft);
                enemySprite.AddFrame(walkLeft);
            }
            if (direction.Y > 0)
            {
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standDown);
                enemySprite.AddFrame(walkDown);
            }
            if (direction.Y < 0)
            {
                enemySprite.ClearFrames();
                enemySprite.AddFrame(standUp);
                enemySprite.AddFrame(walkUp);
            }
            //if (direction.Y > 0 && direction.X < 0)
            //{
            //    enemySprite.ClearFrames();
            //    enemySprite.AddFrame(standDownLeft);
            //    enemySprite.AddFrame(walkDownLeft);
            //}
            if (!(Entity.IsRectOccupied(Player.CharSprite.worldRectangle, enemySprite)))
            {
                enemySprite.Velocity = direction * enemySprite.speed;
                //if ((timemax - timecurrent) <= 0.0)
                //{
                //    effect.Play();
                //    timecurrent = 0.0f;
                //}
            }
        }
        void IAggressiveAI.wander()
        {

        }
        #endregion
    }
}
