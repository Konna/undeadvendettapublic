using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using DataTypes;
namespace UndeadVendetta
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        Texture2D spriteSheet, crabSheet, bearSheet, tavernKeepSheet, blacksmithSheet, titleScreen, mapSpriteSheet, healthBar,statsBar, buttonSheet, slimeSheet, sheepSheet, inventoryBase, itemSheet, inventoryButtonSheet, creditsImage, background;
        #region Hub Menu Texture Declarations
        Texture2D hubBackground;
        Texture2D hubTopBar;
        Texture2D hubButtons;
        Texture2D hubInventory;
        Texture2D hubInventoryButtons;
        #endregion
        SoundEffect SlimeEffect;
        Song townMusic, menuMusic, crabMusic, bearMusic;
        SpriteFont Pericles14;
        public static bool paused = false, inInventory = false;
        public enum gameState { Main, Playing, GameEnded, GameOver}
        public static gameState currentGameState = gameState.Main;
        cutsceneScript introductionScript;
        cutsceneScript tavernScript;
        cutsceneScript beachScript;
        cutsceneScript blacksmithScript;
        cutsceneScript epilogueScript;
        Map testWorld;
        #region test

        public struct SaveGameData
        {
            public Vector2 location;
            public string currentMap;
            public List<Item> currentInventory;
        }

        public struct MapData
        {
            public int x;
            public int y;
            public int tile;
        }
        #endregion
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            this.graphics.PreferredBackBufferWidth = 800;
            this.graphics.PreferredBackBufferHeight = 600;
            //this.graphics.IsFullScreen = true;
            this.graphics.ApplyChanges();
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            #region Textures
            titleScreen = Content.Load<Texture2D>(@"SpriteSheets\Test Splash Screen");
            mapSpriteSheet = Content.Load<Texture2D>(@"SpriteSheets\MapTiles");
            spriteSheet = Content.Load<Texture2D>(@"SpriteSheets\CharacterSheet");
            healthBar = Content.Load<Texture2D>(@"SpriteSheets\StatsSheet");
            statsBar = Content.Load<Texture2D>(@"SpriteSheets\statsBar");
            buttonSheet = Content.Load<Texture2D>(@"SpriteSheets\ButtonsSheet");
            slimeSheet = Content.Load<Texture2D>(@"SpriteSheets\SlimeComplete");
            crabSheet = Content.Load <Texture2D>(@"SpriteSheets\crabSheet");
            sheepSheet = Content.Load<Texture2D>(@"SpriteSheets\sheepSheet");
            bearSheet = Content.Load<Texture2D>(@"SpriteSheets\bearspritesheet");
            tavernKeepSheet = Content.Load<Texture2D>(@"SpriteSheets\tavernKeepSheet");
            blacksmithSheet = Content.Load <Texture2D>(@"SpriteSheets\blacksmithSheet");
            itemSheet = Content.Load<Texture2D>(@"SpriteSheets\itemSheet");
            inventoryBase = Content.Load<Texture2D>(@"SpriteSheets\inventorySheet");
            inventoryButtonSheet = Content.Load<Texture2D>(@"SpriteSheets\InventoryButtonSheet");
            creditsImage = Content.Load<Texture2D>(@"SpriteSheets\Credits");
            background = Content.Load<Texture2D>(@"SpriteSheets\blackscreen");
            #endregion
            #region Hub Menu Textures
            hubBackground = Content.Load<Texture2D>(@"SpriteSheets\HubMenu\Back for Menu");
            hubInventory = Content.Load<Texture2D>(@"SpriteSheets\HubMenu\Menus - Bars");
            hubTopBar = Content.Load<Texture2D>(@"SpriteSheets\HubMenu\Buttons Background");
            hubInventoryButtons = Content.Load<Texture2D>(@"SpriteSheets\HubMenu\InventoryMenuButtons");
            hubButtons = Content.Load<Texture2D>(@"SpriteSheets\HubMenu\HubMenuButtons");
            #endregion
            #region Scripts
            introductionScript = Content.Load<cutsceneScript>("introduction");
            tavernScript = Content.Load<cutsceneScript>("Tavern");
            beachScript = Content.Load <cutsceneScript>("beach");
            blacksmithScript = Content.Load<cutsceneScript>("blackSmithIntro");
            epilogueScript = Content.Load<cutsceneScript>("epilogue");
            #endregion
            #region TestWorld
            testWorld = Content.Load<Map>(@"testWorld");
            TileMap.addMap(testWorld, Content);
            #endregion
            font = Content.Load<SpriteFont>(@"Lindsey");
            SlimeEffect = Content.Load<SoundEffect>(@"Sounds\slimeSound");
            Camera.WorldRectangle = new Rectangle(0, 0, 1600, 1600);
            Camera.ViewPortWidth = 800;
            Camera.ViewPortHeight = 600;
            TileMap.Init(testWorld.mapList);
            Player.Init(new Vector2(100, 300),spriteSheet,0,2);
            EnemyManager.Init(slimeSheet, crabSheet, sheepSheet, bearSheet, tavernKeepSheet, blacksmithSheet, SlimeEffect);
            StatsManager.Init(healthBar, statsBar, new Rectangle(50, 20, Player.CharSprite.Health, 20), new Vector2(50, 20));
            EnemyStatsManager.Init(healthBar, new Rectangle(50, 20, 100, 10), new Vector2(50, 20));
            Bag.Init(itemSheet,hubInventory,hubInventoryButtons,graphics.GraphicsDevice);
            CutsceneManager.init(font, introductionScript.script, tavernScript.script, beachScript.script, blacksmithScript.script, epilogueScript.script, background);
            //visu_EnchantMenu.init(new Vector2(100, 100), inventoryBase, itemSheet);
            PauseMenu.init(buttonSheet, graphics.GraphicsDevice);
            MainMenu.init(buttonSheet, titleScreen, graphics.GraphicsDevice);
            CreditsMenu.init(buttonSheet, creditsImage, graphics.GraphicsDevice);
            HubMenu.init(hubButtons,hubBackground, hubTopBar, graphics.GraphicsDevice);
            GoalManager.Init(font);
            townMusic = Content.Load<Song>(@"Sounds\TownMusic");
            menuMusic = Content.Load<Song>(@"Sounds\awesome");
            crabMusic = Content.Load<Song>(@"Sounds\crabpeople");
            bearMusic = Content.Load<Song>(@"Sounds\Boss Theme");
            AudioManager.Init(townMusic, townMusic, menuMusic, crabMusic, bearMusic);
            //EffectsManager.Initialization(spriteSheet, new Rectangle(0, 288, 2, 2), new Rectangle(0, 256, 32, 32), 3);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            MouseState mouse = Mouse.GetState();
            switch (currentGameState)
            {
                case gameState.Main:
                    IsMouseVisible = true;
                    if (MainMenu.play.isClicked)
                    {  currentGameState = gameState.Playing; }
                    if (MainMenu.credits.isClicked)
                    { currentGameState = gameState.GameEnded; }
                    PauseMenu.mainMenu.isClicked = false;
                    paused = false;
                    MainMenu.Update(mouse);
                    break;
                case gameState.Playing:
                    MainMenu.play.isClicked = false;
                    if (CutsceneManager.cutsceneRunning == false)
                    {
                        if (!paused)
                        {
                            if (!inInventory)
                            {
                                IsMouseVisible = false;
                                if (Keyboard.GetState().IsKeyDown(Keys.Escape) || GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
                                {
                                    paused = true;
                                    PauseMenu.resume.isClicked = false;
                                }
                                if (Keyboard.GetState().IsKeyDown(Keys.I) || GamePad.GetState(PlayerIndex.One).Buttons.Y == ButtonState.Pressed)
                                {
                                    inInventory = true;
                                }
                                TileMap.Update();
                                Player.Update(gameTime);
                                StatsManager.Update(gameTime);
                                EnemyManager.Update(gameTime, graphics.GraphicsDevice);
                                EnemyStatsManager.Update(gameTime);
                                GoalManager.Update(graphics.GraphicsDevice);
                                CutsceneManager.update(gameTime);
                            }
                            else if (inInventory)
                            {
                                IsMouseVisible = true;
                                HubMenu.update(mouse);
                                Bag.update(mouse, Keyboard.GetState());
                            }
                        }
                        else if (paused)
                        {
                            IsMouseVisible = true;
                            HubMenu.update(mouse);
                            PauseMenu.update(mouse);
                            if (PauseMenu.quit.isClicked)
                            {
                                this.Exit();
                            }
                            if (PauseMenu.mainMenu.isClicked) currentGameState = gameState.Main;
                            SaveHandler.update();
                        }
                    }
                    else
                    {
                        CutsceneManager.update(gameTime);
                    }
                    break;
                case gameState.GameEnded:
                    IsMouseVisible = true;
                    CreditsMenu.update(mouse);
                    if (CreditsMenu.play.isClicked) { GoalManager.startNewGame(slimeSheet, crabSheet, sheepSheet, bearSheet, tavernKeepSheet, blacksmithSheet, mapSpriteSheet, inventoryBase, SlimeEffect, testWorld.mapList); currentGameState = gameState.Main; }
                    
                    break;
                case gameState.GameOver:
                    IsMouseVisible = true;
                    CreditsMenu.update(mouse);
                    if (CreditsMenu.play.isClicked)
                    {
                        GoalManager.startNewGame(slimeSheet,crabSheet, sheepSheet, bearSheet, tavernKeepSheet, blacksmithSheet, mapSpriteSheet,inventoryBase,SlimeEffect,testWorld.mapList);
                        currentGameState = gameState.Main;
                    }
                    
                    break;

            }
            AudioManager.Update();
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Gray);
            switch(currentGameState)
            {
                case gameState.Main:
                    spriteBatch.Begin();
                    MainMenu.Draw(spriteBatch);
                    spriteBatch.End();
                    break;
                case gameState.Playing:
                   spriteBatch.Begin();
                    TileMap.Draw(spriteBatch);
                    Player.Draw(spriteBatch, font);
                    EnemyManager.Draw(spriteBatch);
                    EnemyStatsManager.Draw(spriteBatch);
                    StatsManager.Draw(spriteBatch, font);
                    GoalManager.Draw(spriteBatch);
                    CutsceneManager.draw(spriteBatch);
                    if (paused)
                    {
                        HubMenu.draw(spriteBatch);
                        PauseMenu.draw(spriteBatch);
                    }
                    if (inInventory)
                    {
                        HubMenu.draw(spriteBatch);
                        Bag.draw(spriteBatch, font);
                        //visu_EnchantMenu.Draw(spriteBatch);
                    }
                    spriteBatch.End();
                    break;
                case gameState.GameOver:
                    spriteBatch.Begin();
                    CreditsMenu.draw(spriteBatch);
                    spriteBatch.End();
                    break;
                case gameState.GameEnded:
                    spriteBatch.Begin();
                    CreditsMenu.draw(spriteBatch);
                    spriteBatch.End();
                    break;
            }
            base.Draw(gameTime);
        }
    }
}
