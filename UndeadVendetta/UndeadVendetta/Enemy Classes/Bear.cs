﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace UndeadVendetta
{
    class Bear : Enemy
    {
        #region Declarations
        public enum BearAIStates { attack, retreat, chase, regen }
        BearAIStates currentState;
        IBearAI enemyAI = new BearAI();
        #endregion

        #region Constructor
        public Bear(Vector2 worldLocation, Texture2D texture, int textureIndex, int frames, int health, int experiance, int setDamage, float setSpeed, SoundEffect effectSound)
            : base(worldLocation, texture, textureIndex, frames, health, experiance, setDamage, setSpeed, effectSound)
        {
            currentState = BearAIStates.chase;
        }
        #endregion

        #region AI Handling
        public void runAI(float elapsedTime)
        {
            switch (currentState)
            {
                case BearAIStates.chase:
                    {
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) > 45)
                        {
                            enemyAI.chase(enemySprite);
                        }
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) <= 45)
                        {
                            currentState = BearAIStates.attack;
                        }
                        break;
                    }
                case BearAIStates.attack:
                    {
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) <= 45)
                        {
                            enemySprite.Velocity = Vector2.Zero;
                            if ((attackTimeMax - attackTimeCurrent) <= 0.0)
                            {
                                enemyAI.attack(damage);
                                attackTimeCurrent = 0.0f;
                            }
                        }
                        else if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) > 45)
                        {
                            currentState = BearAIStates.chase;
                        }
                        break;
                    }
                case BearAIStates.retreat:
                    {
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) < 300)
                        {
                            enemyAI.retreat(enemySprite, elapsedTime);
                        }
                        break;
                    }
                case BearAIStates.regen:
                    {
                        enemyAI.regen(25, 30f, elapsedTime, enemySprite);
                        break;
                    }

            }
        }
        #endregion

        #region Update & Draw
        public override void update(GameTime gameTime)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            runAI(elapsedTime);
            base.update(gameTime);
        }
        public override void draw(SpriteBatch spriteBatch)
        {
            base.draw(spriteBatch);
        }
        #endregion
    }
}
