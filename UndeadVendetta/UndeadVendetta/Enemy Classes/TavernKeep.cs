﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    class TavernKeep: Enemy
    {
        #region Constructor
        public TavernKeep(Vector2 worldLocation, Texture2D texture, int textureIndex, int frames, int health, int experiance, int setDamage, float setSpeed, SoundEffect effectSound)
            : base(worldLocation, texture, textureIndex, frames, health, experiance, setDamage, setSpeed, effectSound)
        {
            enemySprite.AnimateWhenStopped = false;
        }
        #endregion
        #region Update & Draw
        public override void update(GameTime gameTime)
        {
            base.update(gameTime);
        }
        public override void draw(SpriteBatch spriteBatch)
        {
            base.draw(spriteBatch);
        }
        #endregion
    }
}
