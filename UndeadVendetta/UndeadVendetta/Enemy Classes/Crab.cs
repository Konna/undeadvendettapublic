﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace UndeadVendetta
{
    class Crab : Enemy
    {
        #region Declarations
        public enum AggressiveAIStates { attack, wander, chase}
        AggressiveAIStates currentState;
        IAggressiveAI enemyAI = new AggressiveAI();
        #endregion

        #region Constructor
        public Crab(Vector2 worldLocation, Texture2D texture, int textureIndex, int frames, int health, int experiance, int setDamage, float setSpeed, SoundEffect effectSound)
            : base(worldLocation, texture, textureIndex, frames, health, experiance, setDamage, setSpeed, effectSound)
        {
            currentState = AggressiveAIStates.wander;
        }
        #endregion

        #region AI Handling
        public void runAI()
        {
            switch (currentState)
            {
                case AggressiveAIStates.wander:
                    {
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) < 100)
                        {
                            currentState = AggressiveAIStates.chase;
                        }
                        else
                        {
                            enemySprite.Velocity = Vector2.Zero;
                        }
                        break;
                    }
                case AggressiveAIStates.chase:
                    {
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) > 45)
                        {
                            enemyAI.chase(enemySprite);
                        }
                        else if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) <= 45)
                        {
                            currentState = AggressiveAIStates.attack;
                        }
                        else
                        {
                            currentState = AggressiveAIStates.wander;
                        }
                        break;
                    }
                case AggressiveAIStates.attack:
                    {
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) <= 45)
                        {
                            enemySprite.Velocity = Vector2.Zero;
                            if ((attackTimeMax - attackTimeCurrent) <= 0.0)
                            {
                                enemyAI.attack(damage);
                                attackTimeCurrent = 0.0f;
                            }
                        }
                        else if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) > 45)
                        {
                            currentState = AggressiveAIStates.chase;
                        }
                        break;
                    }
            }
        }
        #endregion
        #region Update & Draw
        public override void update(GameTime gameTime)
        {
            runAI();
            base.update(gameTime);
        }
        public override void draw(SpriteBatch spriteBatch)
        {
            base.draw(spriteBatch);
        }
        #endregion
    }
}
