﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace UndeadVendetta
{
    class Sheep : Enemy
    {
        #region Declarations
        public enum PassiveAIStates {Wander, Retreat}
        PassiveAIStates currentState;
        IPassiveAI enemyAI = new PassiveAI();
        #endregion
        #region Constructor
        public Sheep(Vector2 worldLocation, Texture2D texture, int textureIndex, int frames, int health, int experiance, int setDamage, float setSpeed, SoundEffect effectSound)
            : base(worldLocation, texture, textureIndex, frames, health, experiance, setDamage, setSpeed, effectSound)
        {
            currentState = PassiveAIStates.Wander;
            enemySprite.AnimateWhenStopped = false;
        }
        #endregion
        #region Helper Methods
        public void runAI(float elapsedTime)
        {
            switch (currentState)
            {
                case PassiveAIStates.Wander:
                    {
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) < 100)
                        {
                            currentState = PassiveAIStates.Retreat;
                        }
                        break;
                    }
                case PassiveAIStates.Retreat:
                    {
                        if ((Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) < 100))
                        {
                            enemyAI.retreat(enemySprite, elapsedTime);
                        }
                        else
                        {
                            enemySprite.Velocity = Vector2.Zero;
                        }
                        break;
                    }
            }
        }
        #endregion
        #region Update & Draw
        public override void update(GameTime gameTime)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            runAI(elapsedTime);
            base.update(gameTime);
        }
        public override void draw(SpriteBatch spriteBatch)
        {
            base.draw(spriteBatch);
        }
        #endregion
    }
}
