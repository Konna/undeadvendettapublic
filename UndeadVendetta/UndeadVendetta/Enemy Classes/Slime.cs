﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace UndeadVendetta
{
    class Slime : Enemy
    {
        #region Declarations
        public enum PassiveAIStates { Follow, Wander, Retreat, Attack }
        PassiveAIStates currentState;
        IPassiveAI enemyAI = new PassiveAI();
        bool retreatOnce = false;
        #endregion

        #region Constructor
        public Slime(Vector2 worldLocation, Texture2D texture, int textureIndex, int frames, int health, int experiance, int setDamage, float setSpeed, SoundEffect effectSound)
            : base(worldLocation, texture, textureIndex, frames, health, experiance, setDamage, setSpeed, effectSound)
        {
            currentState = PassiveAIStates.Wander;
        }
        #endregion   
        
        #region Helper Methods
        public void runAI(float elapsedTime)
        {
            switch (currentState)
            {
                case PassiveAIStates.Wander:
                    {
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) <= 100)
                        {
                            currentState = PassiveAIStates.Follow;
                        }
                        break;
                    }
                case PassiveAIStates.Follow:
                    {
                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) > 45)
                        {
                            enemyAI.follow(enemySprite);
                            if ((enemySprite.Health < 100) && (retreatOnce == false))
                            {
                                enemySprite.Velocity = Vector2.Zero;
                                currentState = PassiveAIStates.Retreat;
                            }
                            if (enemySprite.Health <= 30)
                            {
                                currentState = PassiveAIStates.Attack;
                            }
                        }
                        else if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) > 100)
                        {
                            currentState = PassiveAIStates.Wander;
                        }
                        else
                        {
                            enemySprite.Velocity = Vector2.Zero;
                        }
                        break;
                    }
                case PassiveAIStates.Retreat:
                    {
                        if ((Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) < 100))
                        {
                            enemyAI.retreat(enemySprite, elapsedTime);
                        }
                        else
                        {
                            enemySprite.Velocity = Vector2.Zero;
                        }
                        if (enemySprite.Health <= 30)
                        {
                            currentState = PassiveAIStates.Attack;
                        }
                        retreatOnce = true;
                        break;
                    }
                case PassiveAIStates.Attack:
                    {

                        if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) <= 45)
                        {
                            enemySprite.Velocity = Vector2.Zero;
                            if ((attackTimeMax - attackTimeCurrent) <= 0.0)
                            {
                                enemyAI.attack(damage);
                                attackTimeCurrent = 0.0f;
                            }
                        }
                        else if (Vector2.Distance(Player.CharSprite.WorldLocation, enemySprite.WorldLocation) > 45)
                        {
                            currentState = PassiveAIStates.Follow;
                        }
                        break;
                    }
            }
        }
        #endregion

        #region Update & Draw
        public override void update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            runAI(elapsed);
            base.update(gameTime);
        }
        public override void draw(SpriteBatch spriteBatch)
        {
            base.draw(spriteBatch);
        }
        #endregion
    }
}
