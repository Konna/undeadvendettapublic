using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    class EngineInterface
    {

        #region Helpers

        public static List<Rectangle> GenerateTileIndex(Texture2D spriteSheet)
        {
            List<Rectangle> textureIndex = new List<Rectangle>();
            for (int i = 0; i < (spriteSheet.Height / 16); ++i)
            {
                for (int j = 0; j < (spriteSheet.Width / 16); ++j)
                {
                    textureIndex.Add(new Rectangle(j * 16, i * 16, 16, 16));
                }
            }
            return textureIndex;
        }
        public static List<Rectangle> GenerateSpriteIndex(Texture2D spriteSheet)
        {
            List<Rectangle> textureIndex = new List<Rectangle>();
            for (int i = 0; i < (spriteSheet.Height / 32); ++i)
            {
                for (int j = 0; j < (spriteSheet.Width / 32); ++j)
                {
                    textureIndex.Add(new Rectangle(j * 32, i * 32, 32, 32));
                }
            }
            return textureIndex;
        }
        #endregion

    }
}
