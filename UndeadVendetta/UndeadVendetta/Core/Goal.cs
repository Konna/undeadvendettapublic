﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UndeadVendetta
{
    class Goal
    {
        #region Declarations
        private Vector2 startLocation, newLocation;
        private string newMap;
        private bool right, up;
        #endregion

        #region Constructor
        public Goal(Vector2 targetLocation, Vector2 endLocation, string nextMap, bool Up, bool Right)
        {
            startLocation = targetLocation;
            newLocation = endLocation;
            newMap = nextMap;
            right = Right;
            up = Up;
        }
        #endregion

        #region Update
        // TODO: rework!
        public void Update(GraphicsDevice graphics)
        {
            if (right)
            {
                if (up)
                {
                    if (Player.CharSprite.WorldLocation.X >= startLocation.X && Player.CharSprite.WorldLocation.Y <= startLocation.Y)
                    {
                        TileMap.currentMap = newMap;
                        TileMap.Update();
                        TileMap.reDraw(graphics);
                        Player.CharSprite.WorldLocation = newLocation; 
                    }
                }
                else if (!up)
                {
                    if (Player.CharSprite.WorldLocation.X >= startLocation.X && Player.CharSprite.WorldLocation.Y >= startLocation.Y)
                    {
                        TileMap.currentMap = newMap;
                        TileMap.Update();
                        TileMap.reDraw(graphics);
                        Player.CharSprite.WorldLocation = newLocation; 
                    }
                }
            }
            else if (!right)
            {
                if (up)
                {
                    if (Player.CharSprite.WorldLocation.X <= startLocation.X && Player.CharSprite.WorldLocation.Y <= startLocation.Y)
                    {
                        TileMap.currentMap = newMap;
                        TileMap.Update();
                        TileMap.reDraw(graphics);
                        Player.CharSprite.WorldLocation = newLocation; 
                    }
                }
                else if (!up)
                {
                    if (Player.CharSprite.WorldLocation.X <= startLocation.X && Player.CharSprite.WorldLocation.Y >= startLocation.Y)
                    {
                        TileMap.currentMap = newMap;
                        TileMap.Update();
                        TileMap.reDraw(graphics);
                        Player.CharSprite.WorldLocation = newLocation;
                    }
                }
            }
        }
        #endregion
    }
}
