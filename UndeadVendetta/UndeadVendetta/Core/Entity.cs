﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UndeadVendetta
{
    public class Entity : Sprite
    {
        private int health = 0;

        public int Health
        {
            get { return health; }
            set { health = value; }
        }

        public static bool IsRectOccupied(Entity otherPlayer, Entity enemy, Vector2 moveAngle)
        {
            if (!enemy.Expired)
            {
                Rectangle enemyRect = new Rectangle((int)enemy.WorldLocation.X,
                   (int)enemy.WorldLocation.Y, enemy.FrameWidth, enemy.FrameHeight);
                Rectangle otherRect = new Rectangle((int)otherPlayer.WorldLocation.X + (int)moveAngle.X,
                   (int)otherPlayer.WorldLocation.Y + (int)moveAngle.Y, otherPlayer.FrameWidth, otherPlayer.FrameHeight);

                return otherRect.Intersects(enemyRect);
            }
            else
            {
                return false;
            }
        }
        public static bool IsRectOccupied(Rectangle otherPlayer, Entity enemy)
        {
            if (!enemy.Expired)
            {
                Rectangle enemyRect = new Rectangle((int)enemy.WorldLocation.X,
                   (int)enemy.WorldLocation.Y, enemy.FrameWidth, enemy.FrameHeight);
                return otherPlayer.Intersects(enemyRect);
            }
            else
            {
                return false;
            }
        }
        public Entity(Vector2 worldLocation, Texture2D texture, Vector2 velocity, int textureIndex, int frames, int newhealth, float setSpeed)
            : base(worldLocation, texture, velocity, textureIndex, frames, setSpeed)
        {
            Health = newhealth;
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
