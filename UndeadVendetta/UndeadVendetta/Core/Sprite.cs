﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UndeadVendetta
{
    public class Sprite
    {
        #region Declarations

        public Texture2D Texture;

        private Vector2 worldLocation = Vector2.Zero;
        private Vector2 velocity = Vector2.Zero;

        private List<Rectangle> frames = new List<Rectangle>();
        private List<Rectangle> spriteSheetFrames = new List<Rectangle>();

        private float frameTime = 0.1f;
        private float timeForCurrentFrame = 0.0f;
        private float rotation = 0.0f;
        public float speed = 0.0f;

        public enum FrameLoop { UP, DOWN, LEFT, RIGHT };
        private FrameLoop frameSelection = FrameLoop.LEFT;

        private Color tintColor = Color.White;

        public bool Expired = false;
        public bool Animate = true;
        public bool AnimateWhenStopped = true;
        public bool Collidable = true;

        public int CollisionRadius = 0;
        public int BoundingXPadding = 0;
        public int BoundingYPadding = 0;
        private int currentFrame;

        #endregion

        #region Constructors
        public Sprite(
            Vector2 setWorldLocation,
            Texture2D spriteTexture,
            Vector2 setVelocity, int textureIndex, int frames, float setSpeed)
        {
            worldLocation = setWorldLocation;
            Texture = spriteTexture;
            spriteSheetFrames = EngineInterface.GenerateSpriteIndex(spriteTexture);
            velocity = setVelocity;
            speed = setSpeed;
            addAllFrames(textureIndex, frames);
        }
        #endregion

        #region HealperMethods

        public void addAllFrames(int startingFrame, int numOfFrames)
        {
            this.frames.Clear();
            for (int i = startingFrame; i <= (startingFrame + (numOfFrames - 1)); ++i)
            {
                this.frames.Add(spriteSheetFrames[i]);
            }
        }


        #endregion

        #region Drawing and Animation Properties
        public int FrameWidth
        {
            get { return 32; }
        }
        public int FrameHeight
        {
            get { return 32; }
        }
        public Color TintColor
        {
            get { return tintColor; }
            set { tintColor = value; }
        }
        public float Rotation
        {
            get { return rotation; }
            set { rotation = value % MathHelper.TwoPi; }
        }
        public int Frame
        {
            get { return currentFrame; }
            set
            {
                currentFrame = (int)MathHelper.Clamp(value, 0,
                    frames.Count - 1);
            }
        }
        public float FrameTime
        {
            get { return frameTime; }
            set { frameTime = MathHelper.Max(0, value); }
        }
        public Rectangle Source
        {
            get { return frames[currentFrame]; }
        }
        public FrameLoop currentFrameSelection
        {
            get { return frameSelection; }
            set { frameSelection = value; }
        }
        #endregion

        #region Positional Properties
        public Vector2 WorldLocation
        {
            get { return worldLocation; }
            set { worldLocation = value; }
        }

        public Vector2 ScreenLocation
        {
            get
            {
                return Camera.Transform(worldLocation);
            }
        }
        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }
        public Rectangle worldRectangle
        {
            get
            {
                return new Rectangle((int)worldLocation.X, (int)worldLocation.Y, FrameWidth, FrameHeight);
            }
        }
        public Rectangle ScreenRectangle
        {
            get
            {
                return Camera.Transform(worldRectangle);
            }
        }
        public Vector2 RelativeCenter
        {
            get { return new Vector2(FrameWidth / 2, FrameHeight / 2); }
        }
        public Vector2 WorldCenter
        {
            get { return worldLocation + RelativeCenter; }
        }
        public Vector2 ScreenCenter
        {
            get
            {
                return Camera.Transform(worldLocation + RelativeCenter);
            }
        }
        #endregion

        #region Animation-Related Methods
        public void AddFrame(Rectangle frameRectangle)
        {
            frames.Add(frameRectangle);
        }

        public void RotateTo(Vector2 direction)
        {
            Rotation = (float)Math.Atan2(direction.Y, direction.X);
        }
        public void ClearFrames()
        {
            frames.Clear();
        }
        #endregion

        #region Collision Related Properties
        public Rectangle BoundingBoxRect
        {
            get
            {
                return new Rectangle(
                (int)worldLocation.X + BoundingXPadding,
                (int)worldLocation.Y + BoundingYPadding,
                FrameWidth - (BoundingXPadding * 2),
                FrameHeight - (BoundingYPadding * 2));
            }
        }
        #endregion

        #region Collision Detection Methods
        public bool IsBoxColliding(Rectangle OtherBox)
        {
            if ((Collidable) && (!Expired))
            {
                return BoundingBoxRect.Intersects(OtherBox);
            }
            else
            {
                return false;
            }
        }

        public bool IsCircleColliding(Vector2 otherCenter, float otherRadius)
        {
            if ((Collidable) && (!Expired))
            {
                if (Vector2.Distance(WorldCenter, otherCenter) < (CollisionRadius + otherRadius))
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
        public Vector2 checkTileObstacles(
           float elapsedTime,
           Vector2 moveAngle)
        {
            Vector2 newHorizontalLocation = WorldLocation +
                (new Vector2(moveAngle.X, 0) * (speed * elapsedTime));

            Vector2 newVerticalLocation = WorldLocation +
                (new Vector2(0, moveAngle.Y) * (speed * elapsedTime));

            Rectangle newHorizontalRect = new Rectangle(
                (int)newHorizontalLocation.X,
                (int)WorldLocation.Y,
                FrameWidth,
                FrameHeight);

            Rectangle newVerticalRect = new Rectangle(
                (int)WorldLocation.X,
                (int)newVerticalLocation.Y,
                FrameWidth,
                FrameHeight);

            int horizLeftPixel = 0;
            int horizRightPixel = 0;

            int vertTopPixel = 0;
            int vertBottomPixel = 0;

            if (moveAngle.X < 0)
            {
                horizLeftPixel = (int)newHorizontalRect.Left;
                horizRightPixel = (int)worldRectangle.Left;
            }

            if (moveAngle.X > 0)
            {
                horizLeftPixel = (int)worldRectangle.Right;
                horizRightPixel = (int)newHorizontalRect.Right;
            }

            if (moveAngle.Y < 0)
            {
                vertTopPixel = (int)newVerticalRect.Top;
                vertBottomPixel = (int)worldRectangle.Top;
            }

            if (moveAngle.Y > 0)
            {
                vertTopPixel = (int)worldRectangle.Bottom;
                vertBottomPixel = (int)newVerticalRect.Bottom;
            }

            if (moveAngle.X != 0)
            {
                for (int x = horizLeftPixel; x < horizRightPixel; x++)
                {
                    for (int y = 0; y < FrameHeight; y++)
                    {
                        if (TileMap.isWallTileByPixel(
                            new Vector2(x, newHorizontalLocation.Y + y)))
                        {
                            moveAngle.X = 0;
                            break;
                        }
                    }
                    if (moveAngle.X == 0)
                    {
                        break;
                    }
                }
            }

            if (moveAngle.Y != 0)
            {
                for (int y = vertTopPixel; y < vertBottomPixel; y++)
                {
                    for (int x = 0; x < FrameWidth; x++)
                    {
                        if (TileMap.isWallTileByPixel(
                            new Vector2(newVerticalLocation.X + x, y)))
                        {
                            moveAngle.Y = 0;
                            break;
                        }
                    }
                    if (moveAngle.Y == 0)
                    {
                        break;
                    }
                }
            }

            return moveAngle;
        }
        #endregion

        #region Update and Draw Methods
        public virtual void Update(GameTime gameTime)
        {
            if (!Expired)
            {
                float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

                timeForCurrentFrame += elapsed;

                if (Animate)
                {
                    if (timeForCurrentFrame >= frameTime)
                    {
                        if ((AnimateWhenStopped) || (velocity != Vector2.Zero))
                        {
                            switch (frameSelection)
                            {
                                case FrameLoop.RIGHT:
                                    {
                                        currentFrame = (currentFrame + 1) % (frames.Count);
                                        timeForCurrentFrame = 0.0f;
                                        break;
                                    }
                                case FrameLoop.LEFT:
                                    {
                                        currentFrame = (currentFrame + 1) % (frames.Count);
                                        timeForCurrentFrame = 0.0f;
                                        break;
                                    }
                                case FrameLoop.DOWN:
                                    {
                                        currentFrame = (currentFrame + 1) % (frames.Count);
                                        timeForCurrentFrame = 0.0f;
                                        break;
                                    }
                                case FrameLoop.UP:
                                    {
                                        currentFrame = (currentFrame + 1) % (frames.Count);
                                        timeForCurrentFrame = 0.0f;
                                        break;
                                    }
                            }
                        }
                    }
                }
                worldLocation += (velocity * elapsed);
            }
        }
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (!Expired)
            {
                if (Camera.ObjectIsVisible(worldRectangle))
                {
                    spriteBatch.Draw(
                        Texture, ScreenCenter, Source, tintColor, rotation, RelativeCenter,
                        1.0f, SpriteEffects.None, 0.0f);
                }
            }
        }
        #endregion


    }
}
