﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace UndeadVendetta
{
    static class GoalManager
    {
        #region Declarations
        static List<Goal> goals = new List<Goal>();
        static string townGoals, trainingGoals, beachGoals, dungeonGoals;
        static SpriteFont font;
        static Vector2 textLocation = new Vector2(600,16);
        static List<Vector2> startLocations = new List<Vector2>();
        static List<Vector2> newLocations = new List<Vector2>();
        #endregion

        #region Init
        public static void  Init(SpriteFont newFont)
        {
            font = newFont;
            trainingGoals = "Press I or Y,\n Kill Slimes,\n Equip Items";
            townGoals = "Search Town for people,\n head down to Beach,\n Reach Level 2";
            beachGoals = "Stop the noises,\n Kill The Crab People,\n Pick up Secret Item";
            dungeonGoals = "Find Dave, \n Kill Dave, \n Enjoy";
            startLocations.Clear();
            startLocations.Add(new Vector2(680,370)); // #0: training map to town
            startLocations.Add(new Vector2(370, 680)); // #1: town map to beach
            startLocations.Add(new Vector2(680,370));
            startLocations.Add(new Vector2(400, 50));
            newLocations.Clear();
            newLocations.Add(new Vector2(50, 300)); // #0: training map to town
            newLocations.Add(new Vector2(400, 150)); // #1: town map to beach
            newLocations.Add(new Vector2(50,300));
            newLocations.Add(new Vector2(250, 680));
            goals.Clear();
            goals.Add(new Goal(startLocations[0], newLocations[0], "Town", true, true));
            goals.Add(new Goal(startLocations[1], newLocations[1], "Beach", false, true));
            goals.Add(new Goal(startLocations[2], newLocations[2], "Dungeon", true, true));
            goals.Add(new Goal(startLocations[3], newLocations[3], "Town", true, true));
        }
        #endregion

        #region Helper Methods 
        public static void startNewGame(Texture2D slimeTexture, Texture2D crabTexture, Texture2D sheepTexture, Texture2D bearTexture, Texture2D tavernKeepTexture, Texture2D blacksmithTexture, Texture2D tileTexture, Texture2D inventTexture, SoundEffect slimeEffect, List<int> tileTest)
        {
            Player.CharSprite.WorldLocation = new Vector2(100, 200);
            Player.CharSprite.Health = 100;
            Player.CharSprite.Expired = false;
            Player.level = 1;
            Player.experience = 0;
            Camera.Position = Vector2.Zero;
            EnemyManager.Init(slimeTexture, crabTexture, sheepTexture, bearTexture, tavernKeepTexture, blacksmithTexture, slimeEffect);
            TileMap.Init(tileTest);
            Inventory.init(inventTexture);
            EffectsManager.Effects.Clear();
            MediaPlayer.Stop();
            AudioManager.reset();
        }
        #endregion


        #region Update & Draw
        public static void Update(GraphicsDevice graphics)
        {
            if (EnemyManager.allEnemiesClear() && TileMap.currentMap == "Training")
            {
                goals[0].Update(graphics);

            }
            if (TileMap.currentMap == "Town")
            {
                goals[1].Update(graphics);
                if (Inventory.ContainsLemon == true)
                {
                    goals[2].Update(graphics);
                }
                
            }
            if (TileMap.currentMap == "Beach")
            {
                goals[3].Update(graphics);
                if (EnemyManager.allEnemiesClear() && Inventory.ContainsLemon == false)
                    Inventory.addToInventory("Lemon");
            }
            if ((TileMap.currentMap == "Dungeon") && (CutsceneManager.epilogue.sceneEnded == true))
            {
                Game1.currentGameState = Game1.gameState.GameEnded;
            }
        }
        
        public static void Draw(SpriteBatch spriteBatch)
        {
            if (TileMap.currentMap == "Town")
            {
                spriteBatch.DrawString(font, townGoals, textLocation, Color.Black);
                if (EnemyManager.allEnemiesClear())
                {
                    //TODO: spriteBatch.Draw(//Arrow stuff);
                }
            }
            if (TileMap.currentMap == "Training")
            {
                spriteBatch.DrawString(font, trainingGoals, textLocation, Color.White);
            }
            if (TileMap.currentMap == "Beach")
            {
                spriteBatch.DrawString(font, beachGoals, textLocation, Color.Black);
            }
            if (TileMap.currentMap == "Dungeon")
            {
                spriteBatch.DrawString(font, dungeonGoals, textLocation, Color.White);
            }
        }
        #endregion
    }
}
