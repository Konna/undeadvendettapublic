﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Media;

namespace UndeadVendetta
{
    class AudioManager
    {
        private static Song townMusic, trainingMusic, menuMusic, crabMusic, bearMusic;
        private static bool menuPlaying, trainingPlaying, townPlaying, crabPlaying, bearPlaying;

        #region Init
        public static void Init(Song town, Song training, Song menu, Song crab, Song bear)
        {
            townMusic = town;
            trainingMusic = training;
            menuMusic = menu;
            crabMusic = crab;
            bearMusic = bear;
        }
        #endregion

        public static void reset()
        {
            menuPlaying = false;
            trainingPlaying = false;
            townPlaying = false;
            crabPlaying = false;
            bearPlaying = false;
        }
        #region Update
        public static void Update()
        {
                if (Game1.currentGameState == Game1.gameState.Main)
                {
                    if (menuPlaying == false)
                    {
                        MediaPlayer.Play(menuMusic);
                        MediaPlayer.IsRepeating = true;
                        menuPlaying = true;
                    }
                }
                else  if (TileMap.currentMap == "Training")
                {
                    if (trainingPlaying == false)
                    {
                        MediaPlayer.Stop();
                        MediaPlayer.Play(trainingMusic);
                        trainingPlaying = true;
                    }
                }
                else if (TileMap.currentMap == "Town")
                {
                    if (townPlaying == false)
                    {
                        MediaPlayer.Stop();
                        MediaPlayer.Play(townMusic);
                        townPlaying = true;
                    }
                }
                else if (TileMap.currentMap == "Beach")
                {
                    if (crabPlaying == false)
                    {
                        MediaPlayer.Stop();
                        MediaPlayer.Play(crabMusic);
                        crabPlaying = true;
                    }
                    if (EnemyManager.allEnemiesClear() == true)
                    {
                        MediaPlayer.Stop();
                    }
                }
                else if (TileMap.currentMap == "Dungeon")
                {
                    if (bearPlaying == false)
                    {
                        MediaPlayer.Stop();
                        MediaPlayer.Play(bearMusic);
                        bearPlaying = true;
                    }
                }
            
        }
        #endregion
    }
}
