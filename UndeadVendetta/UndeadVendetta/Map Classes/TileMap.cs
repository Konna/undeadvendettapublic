﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace UndeadVendetta
{
    public class TileMap
    {
        #region Declarations
        public const int TileWidth = 16;
        public const int TileHeight = 16;
        public const int MapWidth = 50;
        public const int MapHeight = 50;
        static private List<Tile> tiles = new List<Tile>();
        static private List<Map> maps = new List<Map>();
        static private List<Texture2D> textures = new List<Texture2D>();
        static private List<String> mapNames = new List<String>();
        static public string currentMap;
        static public string previousMap;
        static public int[,] mapSquares = new int[MapWidth, MapHeight];
        static private Random rand = new Random();
        static private List<int> testTiles = new List<int>();
        #endregion

        #region Init
        static public void Init(List<int> setTest) // Initialisation of the class setting all avalible tiles and giving them their texture
        {
            testTiles = setTest;
            tiles.Clear();
            //#region tiles
            //tiles.Add(new Tile(texture, 0, false, false, false, "Death Grass")); // #0: death grass
            //tiles.Add(new Tile(texture, 1, false, true, false, "Death fence")); // #1: death fence
            //tiles.Add(new Tile(texture, 2, false, true, false, "Death Fence Side")); // #2: death fence side
            //tiles.Add(new Tile(texture, 3, false, true, false, "Death Fence Bottom Left")); // #3: death fence bottom left
            //tiles.Add(new Tile(texture, 4, false, true, false, "Death Fence Bottom Right")); // #4: death fence bottom right
            //tiles.Add(new Tile(texture, 5, false, true, false, "Death Fence Top Left")); // #5: death fence top left
            //tiles.Add(new Tile(texture, 6, false, true, false, "Death Fence Top Right")); // #6: death fence top right
            //tiles.Add(new Tile(texture, 7, true, false, false, "Tree Stump Bottom Left")); // #7: treestump bottom left
            //tiles.Add(new Tile(texture, 8, true, false, false, "Tree Stump Bottom Right")); // #8: treestump bottom right
            //tiles.Add(new Tile(texture, 9, true, false, false, "Tree Stump Top Left")); // #9: treestump top left
            //tiles.Add(new Tile(texture, 10, true, false, false, "Tree Stump Top Right")); // #10: treestump top right 
            //tiles.Add(new Tile(texture, 11, false, false, false, "Normal Grass")); // #11: grass 
            //tiles.Add(new Tile(texture, 12, false, true, false, "Grass Fence")); // #12: grass fence
            //tiles.Add(new Tile(texture, 13, false, true, false, "Grass Fence Side")); // #13: grass fence side
            //tiles.Add(new Tile(texture, 14, false, true, false, "Grass Fence Bottom Left")); // #14: grass fence bottom left
            //tiles.Add(new Tile(texture, 15, false, true, false, "Grass Fence Bottom Right")); // #15: grass fence bottom right
            //tiles.Add(new Tile(texture, 16, false, true, false, "Grass Fence Top Left")); // #16: grass fence top left
            //tiles.Add(new Tile(texture, 17, false, true, false, "Grass Fence Top Right")); // #17: grass fence top right
            //tiles.Add(new Tile(texture, 18, true, true, false, "Tavern Sign"));// #18: tavern sign
            //tiles.Add(new Tile(texture, 19, true, true, false, "Wood")); // #19: Wood
            //tiles.Add(new Tile(texture, 20, false, true, false, "Brick"));// #20: Brick
            //tiles.Add(new Tile(texture, 21, true, true, false, "Side Wood")); // #21: Side Wood
            //tiles.Add(new Tile(texture, 22, true, true, false, "Tavern Roof")); // #22: Tavern Roof
            //tiles.Add(new Tile(texture, 23, true, true, false, "Tavern Mid Roof")); // #23 Tavern Mid Roof
            //tiles.Add(new Tile(texture, 24, true, false, false, "Door top")); // #24: Door Top
            //tiles.Add(new Tile(texture, 25, true, false, false, "Door bottom")); // #25: Door Bottom
            //tiles.Add(new Tile(texture, 26, true, true, false, "Glass")); // #26: Glass
            //tiles.Add(new Tile(texture, 27, true, true, false, "Blacksmith Roof")); // #27: Blacksmith Roof
            //tiles.Add(new Tile(texture, 28, true, true, false, "Blacksmith mid Roof")); // #28: BlackSmith mid roof
            //tiles.Add(new Tile(texture, 29, true, false, false, "Blacksmith door top")); // #29: Blacksmith door top
            //tiles.Add(new Tile(texture, 30, true, false, false, "Blacksmith door bottom")); // #30: Blacksmith door bottom
            //tiles.Add(new Tile(texture, 31, true, true, false, "Blacksmith sign part 1")); // #31: Blacksmith sign part 1
            //tiles.Add(new Tile(texture, 32, true, true, false, "Blacksmith sign part 2")); // #32: Blacksmith sign part 2
            //tiles.Add(new Tile(texture, 33, true, true, false, "Blacksmith sign part 3")); // #33: Blacksmith sign part 3
            //tiles.Add(new Tile(texture, 34, true, true, false, "Blacksmith sign part 5")); // #34: Blacksmith sign part 5
            //tiles.Add(new Tile(texture, 35, true, true, false, "Blacksmith sign part 4")); // #35: Blacksmith sign part 4
            //tiles.Add(new Tile(texture, 36, false, false, false, "Sand")); // #36: Sand Tile
            //tiles.Add(new Tile(texture, 37, false, true, false, "Water")); // #37: Water Tile
            //tiles.Add(new Tile(texture, 38, false, false, false, "Sand Water fusion")); // #38: SandLine Tile
            //tiles.Add(new Tile(texture, 39, false, true, false, "Sand Boulder")); // #39: Sand Boulder Tile
            //tiles.Add(new Tile(texture, 40, false, false, false, "stone")); // #40: Stone
            //tiles.Add(new Tile(texture, 41, false, true, false, "stone wall")); // #41: stone wall
            //tiles.Add(new Tile(texture, 42, true, true, false, "fountain bottom left")); // #42: fountain bottom left
            //tiles.Add(new Tile(texture, 43, true, true, false, "Fountain bottom left 2")); // #43: fountain bottom left 2
            //tiles.Add(new Tile(texture, 44, true, true, false, "fountain bottom middle")); // #44: fountain middle
            //tiles.Add(new Tile(texture, 45, true, true, false, "fountain bottom right")); // #45: fountain bottom right
            //tiles.Add(new Tile(texture, 46, true, true, false, "fountain bottom right 2")); // #46: fountain bottom right 2
            //tiles.Add(new Tile(texture, 47, true, true, false, "fountain middle left")); // #47: fountain middle left
            //tiles.Add(new Tile(texture, 48, true, true, false, "fountain middle left 2")); // #48: fountain middle left 2
            //tiles.Add(new Tile(texture, 49, true, true, false, "fountain middle middle")); // #49: fountain middle middle
            //tiles.Add(new Tile(texture, 50, true, true, false, "fountain middle right")); // #50: fountain middle right
            //tiles.Add(new Tile(texture, 51, true, true, false, "fountain middle right 2")); // #51: fountain middle right 2
            //tiles.Add(new Tile(texture, 52, true, true, false, "fountain top left")); // #52: fountain top left 
            //tiles.Add(new Tile(texture, 53, true, true, false, "fountain top left 2")); // #53: fountain top left 2
            //tiles.Add(new Tile(texture, 54, true, true, false, "fountain top middle")); // #54: fount top middle
            //tiles.Add(new Tile(texture, 55, true, true, false, "fountain top right")); // #55: fount top right
            //tiles.Add(new Tile(texture, 56, true, true, false, "fount top right 2")); // #56: fount top right 2
            //tiles.Add(new Tile(texture, 57, true, false, false, "sand path")); //#57: sand path
            //#endregion // Tiles to be converted into file format for convience.
            currentMap = "Training";
            if (currentMap == "Training") loadMap("Training");
            previousMap = currentMap;
        }
        #endregion

        #region Testing Methods
        static public void addMap(Map inputMap, ContentManager content)
        {
            //TODO:Add error conditions
            maps.Add(inputMap);
            mapNames.Add(inputMap.name);
            textures.Add(content.Load<Texture2D>(inputMap.textureName));
        }
        static private void generateMap(List<int> mapTiles)
        {
            mapSquares = convertTilesFromFile(mapTiles);
        }
        static private int findMapIndexViaName(string name)
        {
            for (int i = 0; i < maps.Count; ++i)
            {
                if (maps[i].name == name) return i;
            }
            return -1;
        }
        static private int findTextureIndexViaCurrentMap()
        {
            int i = findMapIndexViaName(currentMap);
            //string currentTexture = maps[i].textureName;
            return i;
            //TODO: check to see if the textures actually match.
        }
        static public void loadMap(string mapName)
        {
            previousMap = currentMap;
            int i = findMapIndexViaName(mapName);
            currentMap = maps[i].name;
            generateMap(maps[i].mapList);
        }
        #endregion

        #region Information about map squares
        static public int GetSquareByPixelX(int pixelX)
        {
            return pixelX / TileWidth;
        }
        static public int GetSquareByPixelY(int pixelY)
        {
            return pixelY / TileHeight;
        }
        static public int GetSquareByPixelXForEnemy(int pixelX)
        {
            return pixelX;
        }
        static public int GetSquareByPixelYForEnemy(int pixelY)
        {
            return pixelY;
        }
        static public Vector2 GetSquareAtPixel(Vector2 pixelLocation)
        {
            return new Vector2(GetSquareByPixelX((int)pixelLocation.X), (GetSquareByPixelY((int)pixelLocation.Y)));
        }
        static public Vector2 GetSquareAtPixelForEnemy(Vector2 pixelLocation)
        {
            return new Vector2(GetSquareByPixelXForEnemy((int)pixelLocation.X), (GetSquareByPixelYForEnemy((int)pixelLocation.Y)));
        }
        static public Vector2 GetSquareCenter(int SquareX, int SquareY)
        {
            //return new Vector2((SquareX * TileWidth) + (TileWidth / 2), (SquareY * TileHeight) + (TileHeight / 2));
            return new Vector2(SquareX + 8 , SquareY + 8);
        }
        static public Vector2 GetSquareCenterForEnemy(int SquareX, int SquareY)
        {
            //return new Vector2((SquareX * TileWidth) + (TileWidth / 2), (SquareY * TileHeight) + (TileHeight / 2));
            return new Vector2(SquareX + 16, SquareY + 16);
        }
        static public Vector2 GetSquareCenter(Vector2 square)
        { 
            return GetSquareCenter(
                (int)square.X, (int)square.Y);
        }
        static public Vector2 GetSquareCenterForEnemy(Vector2 square)
        {
            return GetSquareCenterForEnemy(
                (int)square.X, (int)square.Y);
        }
        static public Rectangle SquareWorldRectangle(int x, int y)
        {
            return new Rectangle(x * TileWidth, y * TileHeight, TileWidth, TileHeight);
        }
        static public Rectangle SquareWorldRectangle(Vector2 square)
        {
            return SquareWorldRectangle((int)square.X, (int)square.Y);
        }
        static public Rectangle SquareScreenRectangle(int x, int y)
        {
            return Camera.Transform(SquareWorldRectangle(x, y));
        }
        static public Rectangle SquareScreenRectangle(Vector2 square)
        {
            return SquareScreenRectangle((int)square.X, (int)square.Y);
        }
        #endregion

        #region Information about Map Tiles
        static public int GetTileAtSquare(int tileX, int tileY) // Returns an int from mapSquares if possible, which in turn has an int related to the index of the tiles list.
        {
            if ((tileX >= 0) && (tileX < MapWidth) &
                (tileY >= 0) && (tileY < MapHeight))
            {
                return mapSquares[tileX, tileY];
            }
            else
            { return -1;}
        }
        static public void SetTileAtSquare(int tileX, int tileY, int tile) //Replaces/sets the int in mapSquares location to the select index from the tile list.
        {
            if ((tileX >= 0) && (tileX < MapWidth) &
                (tileY >= 0) && (tileY < MapHeight))
            { mapSquares[tileX, tileY] = tile;}
        }
        static public int GetTileAtPixel(int pixelX, int pixelY) // Returns an int from mapSquares if possible which in turn is from the index of tiles
        {
            return GetTileAtSquare(GetSquareByPixelX(pixelX), GetSquareByPixelY(pixelY));
        }
        static public int GetTileAtPixel(Vector2 pixelLocation)
        {
            return GetTileAtPixel((int)pixelLocation.X, (int)pixelLocation.Y);
        }
        static public bool isWallTile(int tileX, int tileY) // Uses the Tile.isSolid property to check if the tile can be walked through
        {
            if(GetTileAtSquare(tileX,tileY) == -1) return false;
            if (tiles[GetTileAtSquare(tileX, tileY)].isSolid)
            {
                return true;
            }
            return false;
        }
        static public bool isWallTile(Vector2 square) // Runs the isWallTile method with a integers taken from a vector
        {
            return isWallTile((int)square.X, (int)square.Y);
        }
        static public bool isWallTileByPixel(Vector2 pixelLocation)
        {
            return isWallTile(GetSquareByPixelX((int)pixelLocation.X), GetSquareByPixelY((int)pixelLocation.Y));
        }
        #endregion

        #region Draw
        public static void Draw(SpriteBatch spriteBatch)
        {
            int startX = GetSquareByPixelX((int)Camera.Position.X);
            int endX = GetSquareByPixelX((int)Camera.Position.X + Camera.ViewPortWidth);
            int startY = GetSquareByPixelY((int)Camera.Position.Y);
            int endY = GetSquareByPixelY((int)Camera.Position.Y + Camera.ViewPortHeight);

            for (int x = startX; x <= endX; ++x)
            {
                for (int y = startY; y <= endY; ++y)
                {
                    if ((x >= 0) && (y >= 0) && (x < MapWidth) && (y < MapHeight))
                    {
                        spriteBatch.Draw(textures[findTextureIndexViaCurrentMap()], SquareScreenRectangle(x, y), tiles[GetTileAtSquare(x, y)].rectangle, Color.White);
                    }
                }
            }
        }
        public static void reDraw(GraphicsDevice graphics)
        {
            SpriteBatch spriteBatch = new SpriteBatch(graphics);
            int startX = GetSquareByPixelX((int)Camera.Position.X);
            int endX = GetSquareByPixelX((int)Camera.Position.X + Camera.ViewPortWidth);

            int startY = GetSquareByPixelY((int)Camera.Position.Y);
            int endY = GetSquareByPixelY((int)Camera.Position.Y + Camera.ViewPortHeight);

            for (int x = startX; x <= endX; ++x)
            {
                for (int y = startY; y <= endY; ++y)
                {
                    if ((x >= 0) && (y >= 0) && (x < MapWidth) && (y < MapHeight))
                    {
                        spriteBatch.Begin();
                        spriteBatch.Draw(textures[findTextureIndexViaCurrentMap()], SquareScreenRectangle(x, y), tiles[GetTileAtSquare(x, y)].rectangle, Color.White);
                        spriteBatch.End();
                    }
                }
            }
        }
        public static void Update()
        {
            if (currentMap != previousMap)
            {
                loadMap(currentMap);
                if (currentMap == "Training") Camera.Position = Vector2.Zero;
                else if (currentMap == "Town")
                { 
                    Camera.Position = Vector2.Zero;
                    EnemyManager.GenerateTownSheep();
                }
                else if (currentMap == "Beach")
                {
                    Camera.Position = Vector2.Zero;
                    EnemyManager.GenerateBeachEnemies();
                }
                else if (currentMap == "Dungeon")
                {
                    Camera.Position = Vector2.Zero;
                    EnemyManager.GenerateDungeonBoss();
                }
            }
        }
        #endregion

        #region HelperMethods

        static private bool CheckTileRange(int x, int y, int min, int max) // Returns true if the tile pasted through is be between a certain range
        {
            for (int i = min; i <= max; ++i)
            {
                if (mapSquares[x, y] == i)
                {
                    return true;
                }
            }
            return false;
        }

        static private int[,] convertTilesFromFile(List<int> fileTiles)
        {
            int[,] newMap = new int[50,50];
            // TestCode remove this when map is complete
            for (int xx = 0; xx < 50; ++xx)
            {
                for (int yy = 0; yy < 50; ++yy)
                {
                    newMap[xx, yy] = 0;
                }
            }

            // endTestCode
            int x = 0;
            int y = 0;
            for(int i = 0; i < 52; ++i)
            {
                if (i <= 49)
                {
                    x = i;
                    y = 0;
                }
                else if (i >= 50 && i % 50 == 0)
                { 
                    ++y;
                    x = i - (50 * y);
                }
                newMap[x,y] = fileTiles[i];
            }  
            return newMap;
        }
        #endregion

        #region MAP Generation
        #region basically redundant code

        #region Town Map
        public static void GenerateTownMap()
        {
            int grassTile = 11;
            int fenceTile = 12;
            int sidefenceTile = 13;
            int tavernTop = 22;
            int tavernMid = 23;
            int brick = 20;
            int tavernSign = 18;
            int wood = 19;
            int sideWood = 21;
            int doorTop = 24;
            int doorBottom = 25;
            int blacksmithRoof = 27;
            int blacksmithRoofMid = 28;
            for (int x = 0; x < MapWidth; x++)
            {
                for (int y = 0; y < MapHeight; y++)
                {

                    if (!(tiles[GetTileAtSquare(x, y)].isSpecialTile)) // Stops special Tiles being replaced
                    {

                        if ((x == 0) || (x == MapWidth - 1))
                        {
                            mapSquares[x, y] = sidefenceTile;
                            continue;
                        }
                        if ((y == 0) || (y == MapHeight - 1))
                        {
                            mapSquares[x, y] = fenceTile;
                            continue;
                        }
                        if ((x > 0 && x <= 11) && (y >= 2 && y <= 4))
                        {
                            mapSquares[x, y] = tavernMid;
                            continue;
                        }
                        if ((x == 1 || x == 11) && (y >= 5 && y <= 9))
                        {
                            mapSquares[x, y] = wood;
                            continue;
                        }
                        if ((x >= 4 && x <= 5) && (y >= 8 && y <= 9))
                        {
                            if (y == 8)
                                mapSquares[x, y] = doorTop;
                            if (y == 9)
                                mapSquares[x, y] = doorBottom;
                            continue;
                        }
                        if ((x == 6) && (y == 8))
                        {
                            mapSquares[x, y] = tavernSign;
                            continue;
                        }
                        if ((x >= 2 && x <= 21) && (y >= 7 && y <= 9 || y == 5))
                        {
                            mapSquares[x, y] = brick;
                            continue;
                        }
                        if ((x == 1 || x == 11 || x == 12 || x == 22) && (y >= 5 && y <= 9))
                        {
                            mapSquares[x, y] = wood;
                            continue;
                        }
                        
                        
                        if ((x >= 12 && x <= 22) && (y == 1))
                        {
                            mapSquares[x, y] = blacksmithRoof;
                            continue;
                        }
                        if ((x >= 12 && x <= 22) && (y >= 2 && y <= 4))
                        {
                            mapSquares[x, y] = blacksmithRoofMid;
                            continue;
                        }
                        if (((x >= 2 && x <= 10) || (x >= 13 && x <= 21)) && (y == 6))
                        {
                            mapSquares[x, y] = sideWood;
                            continue;
                        }
                        if ((x > 0 && x <= 11) && (y == 1))
                        {
                            mapSquares[x, y] = tavernTop;
                            continue;
                        }

                        if (x == 23 && y == 23)
                        {
                            mapSquares[x, y] = 52;
                            for (int j = 1; j < 5; j++)
                            {
                                mapSquares[x + j, y] = 52 + j;
                            }
                            mapSquares[x, y + 1] = 47;
                            for (int j = 1; j < 5; j++)
                            {
                                mapSquares[x + j, 24] = 47 + j;
                            }
                            mapSquares[x, y + 2] = 42;
                            for (int j = 1; j < 5; j++)
                            {
                                mapSquares[x + j, 25] = 42 + j;
                            }
                            continue;

                        }
                        mapSquares[x, y] = grassTile;
                    }
                }
            }
        }
        #endregion

        #endregion
        #endregion
    }
}
