﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace UndeadVendetta
{
    class Tile
    {
        #region Declarations
        static Texture2D texture;
        public List<Rectangle> spriteSheetTiles;
        public Rectangle rectangle;
        static Vector2 size = new Vector2(16, 16);
        static String name;
        public bool isSpecialTile, isSolidTile, isTeleportTile;
        #endregion

        #region Constructor
        public Tile(Texture2D newTexture, int textureIndex, bool isSpecial, bool isSolid, bool isTeleport, string newName) // standard Constructor
        {
            texture = newTexture;
            spriteSheetTiles = EngineInterface.GenerateTileIndex(newTexture);
            rectangle = spriteSheetTiles[textureIndex];
            isSpecialTile = isSpecial;
            isSolidTile = isSolid;
            isTeleportTile = isTeleport;
            name = newName;
        }
        #endregion

        #region Properties
        public bool isSpecial // returns whether the Tile can be replaced on the map
        {
            get { return isSpecialTile; }
        }

        public bool isSolid // returns whether the Player or Enemy can move through the Tile
        {
            get { return isSolidTile; }
        }

        public String tileName // returns the name of the Tile
        {
            get { return name; }
        }
        public Rectangle tileRectangle // returns the Rectangle of the Tile
        {
            get { return rectangle; }
            set { rectangle = value; }
        }
        #endregion
    }
}
