﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System.Diagnostics;

namespace UndeadVendetta
{
    class SaveHandler
    {
        
        #region Declarations
        
        enum SavingState
        {
            NotSaving,
            ReadyToSelectStorageDevice,
            SelectingStorageDevice,

            ReadyToOpenStorageContainer,    // once we have a storage device start here
            OpeningStorageContainer,
            ReadyToSave
        }
        static KeyboardState oldKeyboardState;
        static KeyboardState currentKeyboardState;
        static StorageDevice storageDevice;
        static SavingState savingState = SavingState.NotSaving;
        static IAsyncResult asyncResult;
        static PlayerIndex playerIndex = PlayerIndex.One;
        static StorageContainer storageContainer;
        static string filename = "savegame.sav";

        public static Game1.SaveGameData saveGameData = new Game1.SaveGameData()
        {
            location = new Vector2(100, 200),
            currentMap = "Training"
        };
        #endregion

        #region Update
        public static void update()
        {
            oldKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            updateSaveKey(Keys.F1);
            updateSaving();
        }
        #endregion
        private static void updateSaveKey(Keys saveKey)
        {
            if (!oldKeyboardState.IsKeyDown(saveKey) && currentKeyboardState.IsKeyDown(saveKey))
            {
                if (savingState == SavingState.NotSaving)
                {
                    saveGameData.currentMap = TileMap.currentMap;
                    saveGameData.location = Player.CharSprite.ScreenLocation;
                    saveGameData.currentInventory = Inventory.inventory;
                    savingState = SavingState.ReadyToOpenStorageContainer;
                }
            }
        }

        private static void updateSaving()
        {
            switch (savingState)
            {
                case SavingState.ReadyToSelectStorageDevice:
                    {
                        asyncResult = StorageDevice.BeginShowSelector(playerIndex, null, null);
                        savingState = SavingState.SelectingStorageDevice;
                    }
                    break;

                case SavingState.SelectingStorageDevice:
                    if (asyncResult.IsCompleted)
                    {
                        storageDevice = StorageDevice.EndShowSelector(asyncResult);
                        savingState = SavingState.ReadyToOpenStorageContainer;
                    }
                    break;

                case SavingState.ReadyToOpenStorageContainer:
                    if (storageDevice == null || !storageDevice.IsConnected)
                    {
                        savingState = SavingState.ReadyToSelectStorageDevice;
                    }
                    else
                    {
                        asyncResult = storageDevice.BeginOpenContainer("Game1StorageContainer", null, null);
                        savingState = SavingState.OpeningStorageContainer;
                    }
                    break;

                case SavingState.OpeningStorageContainer:
                    if (asyncResult.IsCompleted)
                    {
                        storageContainer = storageDevice.EndOpenContainer(asyncResult);
                        savingState = SavingState.ReadyToSave;
                    }
                    break;

                case SavingState.ReadyToSave:
                    if (storageContainer == null)
                    {
                        savingState = SavingState.ReadyToOpenStorageContainer;
                    }
                    else
                    {
                        try
                        {
                            deleteExisting();
                            save();
                        }
                        catch (IOException e)
                        {
                            // Replace with in game dialog notifying user of error
                            Debug.WriteLine(e.Message);
                        }
                        finally
                        {
                            storageContainer.Dispose();
                            storageContainer = null;
                            savingState = SavingState.NotSaving;
                        }
                    }
                    break;
            }
        }

        private static void deleteExisting()
        {
            if (storageContainer.FileExists(filename))
            {
                storageContainer.DeleteFile(filename);
            }
        }

        private static void save()
        {
            using (Stream stream = storageContainer.CreateFile(filename))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Game1.SaveGameData));
                    serializer.Serialize(stream, saveGameData);
                }
                catch(Exception e)
                {
                    Exception ie = e.InnerException;
                }  
            }
        }
    }
}
