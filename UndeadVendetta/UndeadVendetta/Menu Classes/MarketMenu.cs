﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class MarketMenu
    {
        #region Declarations
        public static Button purchase, quit, potions, sell;
        static Texture2D itemTextures, backgroundTexture;
        public static List<Rectangle> textureRectangles = new List<Rectangle>();
        static List<Rectangle> buttons = new List<Rectangle>();
        static List<Rectangle> itemLocations = new List<Rectangle>();
        #endregion

        #region Init
        public static void init(Texture2D setItemTexture, Texture2D setBackgroundTexture, Texture2D buttonTexture, GraphicsDevice graphics)
        {
            itemTextures = setItemTexture;
            backgroundTexture = setBackgroundTexture;
            buttons.Clear();
            textureRectangles.Clear();
            textureRectangles.Add(new Rectangle(0, 0, 32, 32)); // #0: ArmourTexture
            textureRectangles.Add(new Rectangle(32, 0, 32, 32)); // #1: WeaponTexture
            textureRectangles.Add(new Rectangle(64, 0, 32, 32)); // #2: ConsumeTexture
            textureRectangles.Add(new Rectangle(96, 0, 32, 32)); // #3: BorderTextue
            textureRectangles.Add(new Rectangle(128, 0, 32, 32)); // #4: lemonTexture
            itemLocations.Clear();
            purchase = new Button(buttonTexture, 0, new Vector2(32, 32), new Vector2(0, 0), graphics);
            quit = new Button(buttonTexture, 1, new Vector2(32, 32), new Vector2(0, 0), graphics);
            potions = new Button(buttonTexture, 2, new Vector2(32, 32), new Vector2(0, 0), graphics);
            sell = new Button(buttonTexture, 4, new Vector2(32, 32), new Vector2(0, 0), graphics);
        }
        #endregion

        #region Update & Draw
        public static void draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            purchase.draw(spriteBatch, buttons);
            sell.draw(spriteBatch, buttons);
            potions.draw(spriteBatch, buttons);
            quit.draw(spriteBatch, buttons);
        }
        public static void update(MouseState mouse, KeyboardState keyboard)
        {
            purchase.update(mouse);
            sell.update(mouse);
            potions.update(mouse);
            quit.update(mouse);
        }
        #endregion
    }
}
