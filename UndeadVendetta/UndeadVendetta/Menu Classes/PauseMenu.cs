﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class PauseMenu
    {
        #region Declarations
        public static Button  resume, quit, settings, mainMenu;

        static Texture2D texture;
        static List<Rectangle> buttons = new List<Rectangle>();
        #endregion

        #region Init
        public static void init(Texture2D newTexture, GraphicsDevice graphics)
        {
            texture = newTexture;
            buttons.Clear();
            buttons.Add(new Rectangle(0, 0, 309, 89)); // #0: resume game
            buttons.Add(new Rectangle(309, 0, 309, 89)); // #1: quit game
            buttons.Add(new Rectangle(618,0, 309, 89)); // #2: settings
            buttons.Add(new Rectangle(2163, 0, 309, 89)); // #3: main menu
            resume = new Button(texture, 0, new Vector2(309, 89) ,new Vector2(250,150), graphics);
            quit = new Button(texture, 1, new Vector2(309, 89), new Vector2(250, 250), graphics);
            settings = new Button(texture, 2, new Vector2(309, 89), new Vector2(250, 450), graphics);
            mainMenu = new Button(texture, 3, new Vector2(309, 89), new Vector2(250, 350), graphics);
        }
        #endregion

        #region Update and Draw
        public static void update(MouseState mouse)
        {
            resume.update(mouse);
            quit.update(mouse);
            settings.update(mouse);
            mainMenu.update(mouse);
                if (resume.isClicked) Game1.paused = false;
        }
        public static void draw(SpriteBatch spriteBatch)
        {
            resume.draw(spriteBatch,buttons);
            quit.draw(spriteBatch,buttons);
            settings.draw(spriteBatch,buttons);
            mainMenu.draw(spriteBatch, buttons);
        }

        #endregion
    }
}
