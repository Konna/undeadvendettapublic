﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class CreditsMenu
    {
        #region Declarations
        public static Button play;
        static Texture2D texture, background;
        static List<Rectangle> buttons = new List<Rectangle>();
        #endregion

        #region Init
        public static void init(Texture2D newTexture, Texture2D Background, GraphicsDevice graphics)
        {
            texture = newTexture;
            background = Background;
            buttons.Clear();
            buttons.Add(new Rectangle(2163, 0, 309, 89)); // #0: play game
            play = new Button(texture, 0, new Vector2(309, 89),  new Vector2(250, 470), graphics);
        }
        #endregion

        #region Update and Draw
        public static void update(MouseState mouse)
        {

            play.update(mouse);

        }
        public static void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, (new Rectangle(0, 0, 800, 600)), Color.White);
            play.draw(spriteBatch, buttons);
        }

        #endregion
    }
}
