﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class HubMenu
    {
        #region Declarations
        public static Button inventory, quests, settings, character;
        static Texture2D texture, bar, background;
        static List<Rectangle> buttons = new List<Rectangle>();
        static Rectangle backgroundRectangle = new Rectangle(0, 0, 1032, 129);
        static Rectangle barRectangle = new Rectangle(0, 129, 800, 671);
        #endregion

        #region Init
        public static void init(Texture2D setTexture, Texture2D setBarBackground, Texture2D setBackground, GraphicsDevice graphics)
        {
            texture = setTexture;
            background = setBackground;
            bar = setBarBackground;
            buttons.Clear();
            buttons.Add(new Rectangle(0, 0, 156, 34));
            buttons.Add(new Rectangle(164, 0, 156, 34));
            buttons.Add(new Rectangle(329, 0, 156, 34));
            buttons.Add(new Rectangle(493, 0, 156, 34));
            inventory = new Button(texture, 0, new Vector2(156, 34), new Vector2(15, 51), graphics);
            quests = new Button(texture, 2, new Vector2(156, 34), new Vector2(186, 51), graphics);
            settings = new Button(texture, 3, new Vector2(156, 34), new Vector2(360, 51), graphics);
            character = new Button(texture, 1, new Vector2(156, 34), new Vector2(550, 51), graphics);
        }
        #endregion

        #region Update & Draw
        public static void update(MouseState mouse)
        {
            inventory.update(mouse);
            quests.update(mouse);
            settings.update(mouse);
            character.update(mouse);
            if (inventory.isClicked == true)
            {
                Game1.inInventory = true;
                Game1.paused = false;
            }
            if (settings.isClicked == true)
            {
                Game1.inInventory = false;
                Game1.paused = true;
            }
        }
        public static void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(bar, barRectangle, Color.White);
            spriteBatch.Draw(background, backgroundRectangle, Color.White);
            inventory.draw(spriteBatch, buttons);
            quests.draw(spriteBatch, buttons);
            settings.draw(spriteBatch, buttons);
            character.draw(spriteBatch, buttons);
        }
        #endregion
    }
}
