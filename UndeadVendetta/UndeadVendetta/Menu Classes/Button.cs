﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class Button
    {

        #region Declarations

        Texture2D texture;
        Vector2 position;
        Rectangle rectangle;
        Color color = new Color(255, 255, 255, 128);
        int textureIndex;
        bool down;
        public Vector2 size;
        public bool isClicked;

        #endregion

        #region Constructor

        public Button(Texture2D newTexture, int indexMenu, Vector2 newPosition, GraphicsDevice graphics)
        {
            texture = newTexture;
            textureIndex = indexMenu;
            size = new Vector2(269, 71);
            position = newPosition;
            rectangle = new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }
        public Button(Texture2D newTexture, int indexMenu,Vector2 buttonSize, Vector2 newPosition, GraphicsDevice graphics)
        {
            texture = newTexture;
            textureIndex = indexMenu;
            size = buttonSize;
            position = newPosition;
            rectangle = new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }
        #endregion

        #region Update and Draw

        public void update(MouseState mouse)
        {

            Rectangle mouseRectangle = new Rectangle((int)mouse.X, (int)mouse.Y, 1, 1);

            if (mouseRectangle.Intersects(rectangle))
            {
                if (color.A == 255) down = false;
                if (color.A == 0) down = true;
                if (down) color.A += 3; else color.A -= 3; 
                if (mouse.LeftButton == ButtonState.Pressed) isClicked = true;
            }
            else if (color.A < 255)
            {
                color.A += 3;
                isClicked = false;
            }
        }

        public void draw(SpriteBatch spriteBatch, List<Rectangle> list)
        {
            spriteBatch.Draw(texture, rectangle, list[textureIndex], color);
        }
        #endregion

    }
}
