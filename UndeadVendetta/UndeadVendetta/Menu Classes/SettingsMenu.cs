﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class SettingsMenu
    {
        #region Declarations
        public static Button back, mainMenu;
        static Texture2D texture;
        static List<Rectangle> buttons = new List<Rectangle>();
        #endregion

        #region Init
        public static void Init(Texture2D newTexture, GraphicsDevice graphics)
        {
            texture = newTexture;
            buttons.Clear();
            buttons.Add(new Rectangle(807, 0, 269, 71)); // #0: play game
            buttons.Add(new Rectangle(1076, 0, 269, 71)); // #1: main menu
            back = new Button(texture, 0, new Vector2(250, 250), graphics);
            mainMenu = new Button(texture, 1, new Vector2(250, 350), graphics);
        }
        #endregion

        #region Update and Draw
        public static void Update(MouseState mouse)
        {

            back.update(mouse);
            mainMenu.update(mouse);

        }
        public static void Draw(SpriteBatch spriteBatch)
        {
            back.draw(spriteBatch, buttons);
            mainMenu.draw(spriteBatch, buttons);
        }

        #endregion
    }
}
