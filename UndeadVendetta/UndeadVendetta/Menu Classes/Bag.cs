﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class Bag
    {
        #region Declarations
        public static Button exit, trash, equip, refine;
        static Texture2D itemTexture, baseTexture;
        public static List<Rectangle> textureRectangles = new List<Rectangle>();
        static List<Rectangle> buttons = new List<Rectangle>();
        static List<Rectangle> itemLocations = new List<Rectangle>();
        static bool deleting = false, equipping = false, refining = false;
        static Rectangle backgroundRectangle = new Rectangle(0, 129, 800, 471);
        #endregion

        #region Init
        public static void Init(Texture2D itemTextureSheet, Texture2D baseTextureSheet, Texture2D buttonSheet, GraphicsDevice graphics)
        {
            Inventory.init(itemTextureSheet);
            itemTexture = itemTextureSheet;
            baseTexture = baseTextureSheet;
            buttons.Clear();
            buttons.Add(new Rectangle(229, 0, 229, 71)); // #0: Trash
            buttons.Add(new Rectangle(0, 0, 229, 71)); // #1: Equip
            buttons.Add(new Rectangle(458, 0, 229, 71)); // #2: Refine
            trash = new Button(buttonSheet, 0, new Vector2(229, 71), new Vector2(324, 122), graphics);
            equip = new Button(buttonSheet, 1, new Vector2(229, 71), new Vector2(324, 211), graphics);
            refine = new Button(buttonSheet, 2, new Vector2(229, 71), new Vector2(324, 300), graphics);
            textureRectangles.Clear();
            textureRectangles.Add(new Rectangle(0, 0, 32, 32)); // #0: ArmourTexture
            textureRectangles.Add(new Rectangle(32, 0, 32, 32)); // #1: WeaponTexture
            textureRectangles.Add(new Rectangle(64, 0, 32, 32)); // #2: ConsumeTexture
            textureRectangles.Add(new Rectangle(96, 0, 32, 32)); // #3: BorderTextue
            textureRectangles.Add(new Rectangle(128, 0, 32, 32)); // #4: lemonTexture
            itemLocations.Clear();
            #region ItemLocations
            itemLocations.Add(new Rectangle(35, 165, 32, 32)); // #0: item 0
            itemLocations.Add(new Rectangle(67, 165, 32, 32));
            itemLocations.Add(new Rectangle(99, 165, 32, 32));
            itemLocations.Add(new Rectangle(131, 165, 32, 32));
            itemLocations.Add(new Rectangle(163, 165, 32, 32));
            itemLocations.Add(new Rectangle(195, 165, 32, 32));
            itemLocations.Add(new Rectangle(227, 165, 32, 32));
            itemLocations.Add(new Rectangle(259, 165, 32, 32)); // #7: row 1 end
            itemLocations.Add(new Rectangle(35, 197, 32, 32));
            itemLocations.Add(new Rectangle(67, 197, 32, 32));
            itemLocations.Add(new Rectangle(99, 197, 32, 32));
            itemLocations.Add(new Rectangle(131, 197, 32, 32));
            itemLocations.Add(new Rectangle(163, 197, 32, 32));
            itemLocations.Add(new Rectangle(195, 197, 32, 32));
            itemLocations.Add(new Rectangle(227, 197, 32, 32));
            itemLocations.Add(new Rectangle(259, 197, 32, 32)); // #15: item 15
            itemLocations.Add(new Rectangle(35, 229, 32, 32)); // #0: item 0
            itemLocations.Add(new Rectangle(67, 229, 32, 32));
            itemLocations.Add(new Rectangle(99, 229, 32, 32));
            itemLocations.Add(new Rectangle(131, 229, 32, 32));
            itemLocations.Add(new Rectangle(163, 229, 32, 32));
            itemLocations.Add(new Rectangle(195, 229, 32, 32));
            itemLocations.Add(new Rectangle(227, 229, 32, 32));
            itemLocations.Add(new Rectangle(259, 229, 32, 32)); // #7: row 1 end
            itemLocations.Add(new Rectangle(35, 261, 32, 32));
            itemLocations.Add(new Rectangle(67, 261, 32, 32));
            itemLocations.Add(new Rectangle(99, 261, 32, 32));
            itemLocations.Add(new Rectangle(131, 261, 32, 32));
            itemLocations.Add(new Rectangle(163, 261, 32, 32));
            itemLocations.Add(new Rectangle(195, 261, 32, 32));
            itemLocations.Add(new Rectangle(227, 261, 32, 32));
            itemLocations.Add(new Rectangle(259, 261, 32, 32));
            itemLocations.Add(new Rectangle(35, 293, 32, 32)); // #0: item 0
            itemLocations.Add(new Rectangle(67, 293, 32, 32));
            itemLocations.Add(new Rectangle(99, 293, 32, 32));
            itemLocations.Add(new Rectangle(131, 293, 32, 32));
            itemLocations.Add(new Rectangle(163, 293, 32, 32));
            itemLocations.Add(new Rectangle(195, 293, 32, 32));
            itemLocations.Add(new Rectangle(227, 293, 32, 32));
            itemLocations.Add(new Rectangle(259, 293, 32, 32)); // #7: row 1 end
            itemLocations.Add(new Rectangle(35, 325, 32, 32));
            itemLocations.Add(new Rectangle(67, 325, 32, 32));
            itemLocations.Add(new Rectangle(99, 325, 32, 32));
            itemLocations.Add(new Rectangle(131, 325, 32, 32));
            itemLocations.Add(new Rectangle(163, 325, 32, 32));
            itemLocations.Add(new Rectangle(195, 325, 32, 32));
            itemLocations.Add(new Rectangle(227, 325, 32, 32));
            itemLocations.Add(new Rectangle(259, 325, 32, 32));
            itemLocations.Add(new Rectangle(35, 357, 32, 32)); // #0: item 0
            itemLocations.Add(new Rectangle(67, 357, 32, 32));
            itemLocations.Add(new Rectangle(99, 357, 32, 32));
            itemLocations.Add(new Rectangle(131, 357, 32, 32));
            itemLocations.Add(new Rectangle(163, 357, 32, 32));
            itemLocations.Add(new Rectangle(195, 357, 32, 32));
            itemLocations.Add(new Rectangle(227, 357, 32, 32));
            itemLocations.Add(new Rectangle(259, 357, 32, 32)); // #7: row 1 end
            itemLocations.Add(new Rectangle(35, 389, 32, 32));
            itemLocations.Add(new Rectangle(67, 389, 32, 32));
            itemLocations.Add(new Rectangle(99, 389, 32, 32));
            itemLocations.Add(new Rectangle(131, 389, 32, 32));
            itemLocations.Add(new Rectangle(163, 389, 32, 32));
            itemLocations.Add(new Rectangle(195, 389, 32, 32));
            itemLocations.Add(new Rectangle(227, 389, 32, 32));
            itemLocations.Add(new Rectangle(259, 389, 32, 32));
            #endregion
        }
        #endregion

        #region Helper Methods
        public static void trashFromInventory(MouseState mouse)
        {
            Rectangle mouseRectangle = new Rectangle((int)mouse.X, (int)mouse.Y, 1, 1);
            for (int i = 0; i < itemLocations.Count; i++)
            {
                if (Inventory.inventory.Count > i)
                {
                    if (mouseRectangle.Intersects(itemLocations[i]))
                    {
                        if (mouse.LeftButton == ButtonState.Pressed)
                        {
                            Inventory.removeFromInventory(i);
                            deleting = false;
                        }
                    }
                }

            }
        }
        public static void refineItem(MouseState mouse)
        {
            Rectangle mouseRectangle = new Rectangle((int)mouse.X, (int)mouse.Y, 1, 1);
            for (int i = 0; i < itemLocations.Count; i++)
            {
                if (Inventory.inventory.Count > i)
                {
                    if (mouseRectangle.Intersects(itemLocations[i]))
                    {
                        if (mouse.LeftButton == ButtonState.Pressed)
                        {
                            Inventory.refineItem(i);
                            refining = false;
                        }
                    }
                }

            }
        }
        public static void equipFromInventory(MouseState mouse)
        {
            Rectangle mouseRectangle = new Rectangle((int)mouse.X, (int)mouse.Y, 1, 1);
            for (int i = 0; i < itemLocations.Count; i++)
            {
                if (Inventory.inventory.Count > i)
                {
                    if (mouseRectangle.Intersects(itemLocations[i]))
                    {
                        if (mouse.LeftButton == ButtonState.Pressed)
                        {
                            switch (Inventory.inventory[i].Type)
                            {
                                case "Armour":
                                    {
                                        Inventory.equipArmour(i);
                                        break;
                                    }
                                case "Weapon":
                                    {
                                        Inventory.equipWeapon(i);
                                        break;
                                    }
                                case "Consumable":
                                    {
                                        if (Player.CharSprite.Health < 100)
                                        {
                                            if (Player.CharSprite.Health + Inventory.inventory[i].ItemValue > 100)
                                            {
                                                Player.CharSprite.Health = 100;
                                            }
                                            else
                                            {
                                                Player.CharSprite.Health += Inventory.inventory[i].ItemValue;
                                            }
                                            Inventory.removeFromInventory(i);
                                        }
                                        break;
                                    }
                            }
                            equipping = false;
                        }
                    }
                }

            }
        }
        #endregion

        #region Draw & Update
        public static void draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            string attackdisplay = "0";
            string armourdisplay = "0";
            for (int i = 0; i < Inventory.inventory.Count; i++)
            {
                if (Inventory.inventory[i].isEquipped == true)
                {
                    if (Inventory.inventory[i].Type == "Weapon")
                    {
                        attackdisplay = Inventory.inventory[i].ItemValue.ToString();
                    }
                    if (Inventory.inventory[i].Type == "Armour")
                    {
                        armourdisplay = Inventory.inventory[i].ItemValue.ToString();
                    }
                }


            }
            string damageoutput = "Your attack is " + attackdisplay;
            string defenceoutput = "Your Defence is " + armourdisplay;
            spriteBatch.Draw(baseTexture, backgroundRectangle, Color.White);
            spriteBatch.DrawString(font, damageoutput, new Vector2(565, 160), Color.Black);
            spriteBatch.DrawString(font, defenceoutput, new Vector2(565, 180), Color.Black);
            trash.draw(spriteBatch, buttons);
            equip.draw(spriteBatch, buttons);
            refine.draw(spriteBatch, buttons);
            Rectangle textureRectangle;
            int armourCount = 0;
            int weaponCount = 0;
            for (int i = 0; i < Inventory.inventory.Count; i++)
            {
                switch (Inventory.inventory[i].Type)
                {
                    case "Armour":
                        {
                            textureRectangle = textureRectangles[0];
                            break;
                        }
                    case "Weapon":
                        {
                            textureRectangle = textureRectangles[1];
                            break;
                        }
                    case "Lemon":
                        {
                            textureRectangle = textureRectangles[4];
                            break;
                        }

                    default:
                        {
                            textureRectangle = textureRectangles[2];
                            break;
                        }
                }
                spriteBatch.Draw(itemTexture, itemLocations[i], textureRectangle, Color.White);
                if (Inventory.inventory[i].isEquipped)
                {
                    if (Inventory.inventory[i].Type == "Weapon" && weaponCount == 0)
                    {
                        weaponCount += 1;
                        spriteBatch.Draw(itemTexture, itemLocations[i], textureRectangles[3], Color.White);
                    }
                    if (Inventory.inventory[i].Type == "Armour" && armourCount == 0)
                    {
                        armourCount += 1;
                        spriteBatch.Draw(itemTexture, itemLocations[i], textureRectangles[3], Color.White);
                    }
                }
            }

        }
        public static void update(MouseState mouse, KeyboardState keyBoard)
        {
            trash.update(mouse);
            equip.update(mouse);
            refine.update(mouse);
            if (deleting == false)
            {
                if (equipping == false)
                {
                    if (equip.isClicked)
                    {
                        equipping = true;
                    }
                }
                else if (equipping == true)
                {
                    equipFromInventory(mouse);
                }
                if (refining == false)
                {
                    if (refine.isClicked) refining = true;
                }
                else if (refining == true)
                {
                    refineItem(mouse);
                }
                if (trash.isClicked)
                {
                    deleting = true;
                }
            }
            else if (deleting == true)
            {
                trash.isClicked = false;
                if (trash.isClicked)
                {
                    deleting = false;
                }
                trashFromInventory(mouse);
            }

        }
        #endregion
    }
}
