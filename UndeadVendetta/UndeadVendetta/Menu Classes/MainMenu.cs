﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class MainMenu
    {
        #region Declarations
        public static Button play, credits; // quit;
        static Texture2D texture, background;
        static List<Rectangle> buttons = new List<Rectangle>();
        #endregion

        #region Init
        public static void init(Texture2D newTexture, Texture2D Background, GraphicsDevice graphics)
        {
            texture = newTexture;
            background = Background;
            buttons.Clear();
            buttons.Add(new Rectangle(927, 0, 309, 89)); // #0: play game
            buttons.Add(new Rectangle(1545, 0, 309, 89)); // #1: credits
            // buttons.Add(new Rectangle(807, 0, 269, 72)); // #2: quit
            play = new Button(texture, 0, new Vector2(250, 250), graphics);
            credits = new Button(texture, 1, new Vector2(250, 350), graphics);
            //quit = new Button(texture, 2, new Vector2(250, 450), graphics);
        }
        #endregion

        #region Update and Draw
        public static void Update(MouseState mouse)
        {
            play.update(mouse);
            credits.update(mouse);
            //quit.update(mouse);
      
            
        }
        public static void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, (new Rectangle(0, 0, 800, 600)), Color.White);
            play.draw(spriteBatch, buttons);
            credits.draw(spriteBatch, buttons);
            //quit.Draw(spriteBatch, buttons);
        }

        #endregion
    }
}
