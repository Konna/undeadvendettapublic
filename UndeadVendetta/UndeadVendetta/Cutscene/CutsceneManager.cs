﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using System.IO;
using System.Xml.Serialization;

namespace UndeadVendetta
{
    class CutsceneManager
    {
        #region Declarations
        private static Vector2 location = new Vector2(10, 200);
        public static Cutscene introduction, blacksmithIntro, tavernIntro, beachScene, epilogue;
        public static bool cutsceneRunning = false;
        public static Texture2D background;
        #endregion

        #region Init
        public static void init(SpriteFont setFont, List<string> introductionScript, List<string> tavernScript, List<string> beachScript, List<string> blacksmithScript, List<string> epilogueScript, Texture2D setBackground)
        {
            introduction = new Cutscene(setFont, 2.5f, location, introductionScript);
            tavernIntro = new Cutscene(setFont, 2.5f, location, tavernScript);
            blacksmithIntro = new Cutscene(setFont, 2.5f, location, blacksmithScript);
            beachScene = new Cutscene(setFont, 2.5f, location, beachScript);
            epilogue = new Cutscene(setFont, 2.5f, location, epilogueScript);
            background = setBackground;
        }
        #endregion

        #region Helper Methods
        
        #endregion

        #region Update & Draw
        public static void update(GameTime gameTime)
        {
            if ((TileMap.currentMap == "Training") && (introduction.sceneEnded == false))
            {
                introduction.update(gameTime);
                cutsceneRunning = true;
            }
            else if ((TileMap.currentMap == "Town") && (tavernIntro.sceneEnded == false))
            {
                tavernIntro.update(gameTime);
                cutsceneRunning = true;
            }
            else if ((Inventory.ContainsLemon == true) && (beachScene.sceneEnded == false))
            {
                beachScene.update(gameTime);
                cutsceneRunning = true;
            }
            else if ((TileMap.currentMap == "Dungeon") && (EnemyManager.allEnemiesClear() == true) && (blacksmithIntro.sceneEnded == false))
            {
                blacksmithIntro.update(gameTime);
                cutsceneRunning = true;
            }
            else if ((TileMap.currentMap == "Town") && (blacksmithIntro.sceneEnded == true) && (epilogue.sceneEnded == false))
            {
                epilogue.update(gameTime);
                cutsceneRunning = true;
            }
            else
            {
                cutsceneRunning = false;
            }
        }
        public static void draw(SpriteBatch spriteBatch)
        {
            if ((TileMap.currentMap == "Training") && (introduction.sceneEnded == false))
            {
                if (introduction.currentSentenceIndex <= 10) spriteBatch.Draw(background, new Rectangle(0, 0, 800, 600), Color.White);
                introduction.draw(spriteBatch);
            }
            if ((TileMap.currentMap == "Town") && (tavernIntro.sceneEnded == false))
            {
                tavernIntro.draw(spriteBatch);
            }
            if ((Inventory.ContainsLemon == true) && (beachScene.sceneEnded == false))
            {
                beachScene.draw(spriteBatch);
            }
            if ((TileMap.currentMap == "Dungeon") && (EnemyManager.allEnemiesClear() == true) && (blacksmithIntro.sceneEnded == false))
            {
                blacksmithIntro.draw(spriteBatch);
            }
            if ((TileMap.currentMap == "Town") && (blacksmithIntro.sceneEnded == true) && (epilogue.sceneEnded == false))
            {
                epilogue.draw(spriteBatch);
            }
        }
        #endregion
    }
}
