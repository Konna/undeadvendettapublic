﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace UndeadVendetta
{
    class Cutscene
    {
        #region Declarations
        private float sentenceGap = 0.0f, currentGap, firstGap = 1.0f;
        private SpriteFont font;
        private List<string> sceneSentences = new List<string>();
        private string currentSentence;
        public int currentSentenceIndex;
        private Vector2 textLocation;
        public bool sceneEnded = false;
        #endregion

        #region Constructor
        public Cutscene(SpriteFont setFont, float setGap, Vector2 setLocation, List<string> setScene)
        {
            font = setFont;
            sentenceGap = setGap;
            textLocation = setLocation;
            sceneSentences = setScene;
            currentSentenceIndex = 0;
            currentSentence = sceneSentences[currentSentenceIndex];
        }
        #endregion

        #region Update & Draw
        public void update(GameTime gameTime)
        {
            if (sceneEnded == false)
            {
                float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
                currentSentence = sceneSentences[currentSentenceIndex];
                currentGap += elapsedTime;
                if (currentSentenceIndex == 0)
                {
                    if ((firstGap - currentGap) <= 0.0f)
                    {
                        ++currentSentenceIndex;
                        currentGap = 0.0f;
                        
                    }
                }
                else
                {
                    if ((sentenceGap - currentGap) <= 0.0f)
                    {

                        if (currentSentenceIndex == (sceneSentences.Count - 1))
                        {
                            sceneEnded = true;
                        }
                        else
                        {
                            ++currentSentenceIndex;
                        }
                        currentGap = 0.0f;
                    }
                }
                
            }
        }

        public void draw(SpriteBatch spriteBatch)
        {
                spriteBatch.DrawString(font, currentSentence, textLocation, Color.White);
        }
        #endregion
    }
}
