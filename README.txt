Undead Vendetta 13w23a

Things need to do
1) Clean up the player code
2) Build tavern
3) Goal Manager arrows to point to open areas
4) Menu of menus (eqiup, enchant, inventory, stats, quests)
5) Quest :- Save Derek the blacksmith

Things in progess
1) Enchantments
2) Level editor
3) Sprite changes to enable enemies to face different directions 

Enchantments v0.0.1 :-
- Can get a random enchantment from main list of enchantments
- Adds three kinds of enchantment (fire, ice, resistance)
- No control over what kind of eqiupment gets what kind of enchantment i.e. sword with resistance

Level editor v0.0.1 :-
- Grabs textiles from texture based on size of texture and tiles
- Generates map of a certain size
- Plan is to save to .MAP

Sprite v1.0.1 :-
- Directional booleans choose frames from a list

Keybindings v0.0.1 :-
- Plan is to have default keys and variable keys 

Save Data v0.0.1 :-
- Plan is to save the game in .sav files

Map Conversion v0.0.1 :-
- Plan is to convert current map files to .MAP format then load into game